import React, { Component } from 'react'
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { Provider } from 'react-redux';
import configureStore from './store';
import { StatusBar } from 'react-native';
import { Icon } from 'native-base';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import Story from './src/pages/Story';
import Search from './src/pages/Search';
import Home from './src/pages/Home';
import ArticleList from './src/pages/ArticleList';
import Profile from './src/pages/Profile';
import ProfileContent from './src/pages/ProfileContent';
import SignUp from './src/pages/SignUp';
import SignIn from './src/pages/SignIn';
import CommunityDetail from './src/pages/CommunityDetail';
import AddStory from './src/pages/AddStory';
import StoryDetail from './src/pages/StoryDetail';
import UserDataById from './src/pages/UserDataById';
import ArticleDetail from './src/pages/ArticleDetail';
import EditStory from './src/pages/EditStory';
import UpdateProfile from './src/pages/UpdateProfile';
import MyProfile from './src/pages/MyProfile';
import KontenByCategory from './src/pages/KontenByCategory';
import StoryByCategory from './src/pages/StoryByCategory';
import StorySearch from './src/pages/StorySearch';
import KontenSearch from './src/pages/KontenSearch';


const store = configureStore();


const HomeNav = createStackNavigator({
  Home: {
    screen: Home
  },
  Story: {
    screen: Story
  },
  Article: {
    screen: ArticleList
  },
  CommunityDetail: {
    screen: CommunityDetail
  },
  AddStory: {
    screen: AddStory
  },
  StoryDetail: {
    screen: StoryDetail
  },
  UserDataById: {
    screen: UserDataById
  },
  ArticleDetail: {
    screen: ArticleDetail
  },
},
  {
    headerMode: 'none'
  }
)

const SearchNav = createStackNavigator({

  StorySearch: {
    screen: StorySearch
  },
  KontenSearch:{
    screen:KontenSearch
  },
  // Search: {
  //   screen: Search
  // },
  KontenByCategory: {
    screen: KontenByCategory
  },
  StoryByCategory: {
    screen: StoryByCategory
  },
  StoryDetail: {
    screen: StoryDetail
  },
  UserDataById: {
    screen: UserDataById
  },
  ArticleDetail: {
    screen: ArticleDetail
  },
},

  {
    headerMode: 'none'
  }
)

const ProfileNav = createStackNavigator({
  Profile: {
    screen: Profile
  },
  SignIn: {
    screen: SignIn
  },
  Home: {
    screen: Home
  },
  SignUp: {
    screen: SignUp
  },
  ProfileContent: {
    screen: ProfileContent
  },
  EditStory: {
    screen: EditStory
  },
  UpdateProfile: {
    screen: UpdateProfile
  },
  MyProfile: {
    screen: MyProfile
  }
},
  {
    headerMode: 'none'
  }
);
const bottomTab = createBottomTabNavigator(
  {
    HomeTab: {
      screen: HomeNav,
      navigationOptions: {
        title: 'Home',
        tabBarIcon: ({ tintColor }) => (
          <Icon
            type='Ionicons'
            name='md-home'
            style={{
              color: tintColor
            }}
          />
        )
      }
    },
    SearchTab: {
      screen: SearchNav,
      navigationOptions: {
        title: 'Search',
        tabBarIcon: ({ tintColor }) => (
          <Icon
            type='Ionicons'
            name='md-search'
            style={{
              color: tintColor
            }}
          />
        )
      }
    },
    ProfileTab: {
      screen: ProfileNav,
      navigationOptions: {
        title: 'Profile',
        tabBarIcon: ({ tintColor }) => (
          <Icon
            type='Ionicons'
            name='md-person'
            style={{
              color: tintColor
            }}
          />
        )
      }
    },
  },
  {
    initialRouteKey: 'Home',
    tabBarOptions: {
      activeTintColor: '#22cade',
      inactiveTintColor: '#333333',
    }
  }
)

const AppContainer = createAppContainer(bottomTab)

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <StatusBar
          hidden={true} />
        <AppContainer />
      </Provider>
    )
  }
}