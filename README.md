# Project Title
REDU

This is a project of Glints Academy done in a group. This is a React Native app developed to give user information on recreational places. User can post their own stories visiting other places.

## Getting Started

Please follow the instruction below. 

### Prerequisites

To develop this app, you need to install npm or yarn, Android SDK (9.0 Pie or greater), JDK, and AVD or android device connected to your laptop.

### Installing

after you clone the project, install the required libraries from dependencies by typing the command

```
yarn install
```
or
```
npm install
```
and wait until all the instalations are completed.

## Test the Project

You can open the project's directory in any code editor that suits your taste.


## Deployment

Open a terminal, and get into the project's directory.
Use the command

```
react-native start
```
to open the services, wait until all of the processes are done.

Open a new terminal window and type the command
```
react-native run-android
```

and wait untill it finish building the app.

The app will open in your smartphone or virtual device.

## Built With

* [React-Native](https://facebook.github.io/react-native/) - The Mobile App Library
* [Express JS](https://expressjs.com/) - Back-end API controller
* [Native Base](https://nativebase.io/)- React-Native Library for styling

## Contributing

* [Bangkit Simmabur](#) - React-Native
* [Bagus Kurnia](#) - React-Native
* [Agung Sunarya](#) - Back-End
* [Rifqi Taufiqul Hakim ](#) - Back-End
* [Bianca Belinda ](#) - Front-End
* [Julio Uliandro Sitompul](#) - Front-End

## Versioning

This is still in development with only basic CRUD operation on few pages.
search feature, community page, and categories will be added next.

## Notable Bugs

* You will be logged out of your account randomly after you add a story or a content.
* refreshing or reopening the app will still logged you in to your account, but your user account data will not be displayed.
* adding story with your camera will not upload the picture you take from your camera

## Authors
* [Bangkit Simmabur](#) - React-Native
* [Bagus Kurnia](#) - React-Native
* [Agung Sunarya](#) - Back-End
* [Rifqi Taufiqul Hakim ](#) - Back-End
* [Bianca Belinda ](#) - Front-End
* [Julio Uliandro Sitompul](#) - Front-End

