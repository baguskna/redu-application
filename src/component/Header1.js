import React, { Component } from 'react'
import { Text } from 'react-native'
import { Header, Title } from 'native-base'

export default class Header1 extends Component {
    render() {
        return (
            <Header style={{ backgroundColor: '#015249' }}>
                <Title style={{ fontWeight: 'bold', fontFamily: 'serif' }}>
                    <Text style={{ fontSize: 30, color: '#22cade' }}>R</Text>
                    <Text style={{ fontSize: 20, color: '#ffffff' }}>EDU</Text>
                </Title>
            </Header>
        )
    }
}
