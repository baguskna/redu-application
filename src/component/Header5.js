import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { Header, Title, Left, Body, Right, Item, Button } from 'native-base'
import Icon from 'react-native-ionicons';
import { withNavigation } from 'react-navigation';

class Header5 extends Component {
  render() {
    return (
      <Header transparent
        style={{
          backgroundColor: '#015249',
          marginTop: -25
        }}>
        <Left style={{ flex: 1 }}>
          <Icon
            type="MaterialIcons"
            name="arrow-back"
            style={{
              color: '#ffffff'
            }}
            onPress={
              () => {
                this.props.navigation.pop()
              }
            }
          />
        </Left>

        <Body style={{ flex: 1 }}>
          <Title style={{ fontWeight: 'bold', fontFamily: 'serif' }}>
            <Text style={{ fontSize: 30, color: '#22cade' }}>S</Text>
            <Text style={{ fontSize: 20, color: '#ffffff' }}>EARCH</Text>
          </Title>
        </Body>

        <Right style={{ flex: 1 }}>

        </Right>
      </Header>
    )
  }
}

export default withNavigation(Header5)