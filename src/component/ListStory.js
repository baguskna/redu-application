import React, { Component } from 'react'
import { Text, View, Image, FlatList, Dimensions, ActivityIndicator } from 'react-native'
import { Container, CardItem, Card, Left } from 'native-base'
import { withNavigation } from 'react-navigation'
import Axios from 'axios';
class StoryList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            page: 1,
            refreshing: false,
            loading: false,
            category: '',
        }
    }

    getDataStory = () => {
        try {
            const getStory = async () => await Axios.get(`https://glints-redu.herokuapp.com/api/stories/paginate?page=${this.state.page}`, {
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            this.setState({ loading: true });

            getStory()
                .then(res => {
                    this.setState({
                        data: [...this.state.data, ...res.data.data.docs],
                        refreshing: false,
                        loading: false
                    })
                })
                .catch(err => {
                })
        } catch (e) { }
    }

    renderFooter = () => {
        if (!this.state.loading) return null;

        return (
            <View
                style={{
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    borderColor: "#CED0CE"
                }}
            >
                <ActivityIndicator animating size="large" />
            </View>
        );
    }

    componentDidMount() {
        this.getDataStory()
    }

    handleEnd = () => {
        this.setState(state => ({ page: state.page + 1 }), () => this.getDataStory());
    };

    handleRefresh = () => {
        this.setState(
            {
                page: 1,
                refreshing: true,
                data: []
            },
            () => {
                this.getDataStory();
            }
        );
    };

    render() {
        return (
            <Container style={{ backgroundColor: '#faffff' }}>
                <FlatList
                    data={this.state.data}
                    renderItem={({ item }) => (
                        <View
                            key={item._id}
                            style={{ alignItems: 'center', justifyContent: 'center' }}>
                            <Card
                                style={{
                                    marginVertical: 10,
                                    borderRadius: 200 / 12,
                                    width: Dimensions.get('window').width * 0.95,
                                }}>
                                <CardItem
                                    button
                                    onPress={() => this.props.navigation.navigate('StoryDetail', { id: item._id })}
                                    style={{ borderRadius: 200 / 12 }}>
                                    <Text
                                        style={{
                                            fontSize: 18,
                                            fontFamily: 'Roboto',
                                            fontWeight: 'bold',
                                            color: '#015249'
                                        }}
                                    > {item.title} </Text>
                                </CardItem>
                                <CardItem
                                    cardBody
                                    button
                                    onPress={() => this.props.navigation.navigate('StoryDetail', { id: item._id, photo: item._user.photo.secure_url })}
                                    style={{ marginHorizontal: 10 }}>
                                    <Image
                                        style={{
                                            width: '100%',
                                            height: 300,
                                            borderRadius: 200 / 12,
                                        }}
                                        source={{ uri: item.photo ? item.photo.secure_url : null }}
                                    />
                                </CardItem>
                                <CardItem
                                    button
                                    onPress={() => { this.props.navigation.navigate('UserDataById', { id: item._user._id }) }}
                                    style={{ borderRadius: 200 / 12 }}>
                                    <Left>
                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                            <Image
                                                style={{
                                                    width: 25,
                                                    height: 25,
                                                    borderRadius: 25 / 2
                                                }}
                                                source={{ uri: item._user.photo.secure_url }}
                                            />
                                            <Text
                                                style={{
                                                    fontSize: 15,
                                                    fontFamily: 'Roboto',
                                                    fontWeight: 'bold',
                                                }}
                                            > {item._user.name} </Text>
                                        </View>
                                    </Left>
                                </CardItem>
                            </Card>
                        </View>
                    )}
                    keyExtractor={item => item._id}
                    onEndReached={this.handleEnd}
                    onEndReachedThreshold={1}
                    onRefresh={this.handleRefresh}
                    refreshing={this.state.refreshing}
                    // ListHeaderComponent={this.renderHeader}
                    ListFooterComponent={this.renderFooter}
                />
            </Container>
        )
    }
}
export default withNavigation(StoryList)