import React, { Component } from 'react'
import { Text, View, Dimensions, ToastAndroid, ScrollView, Image, ActivityIndicator,Alert } from 'react-native'
import { Container, Item, Input, CheckBox, Body, ListItem, Button, Header, Title, Left, Right, Picker, Fab } from 'native-base'
import { withNavigation } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';
import Axios from 'axios';
import ImagePicker from 'react-native-image-picker';
import Icon from 'react-native-ionicons';

class AddStory extends Component {
    constructor(props) {
        super(props)
        this.state = {
            title: '',
            body: '',
            category: 'Nature',
            id: '',
            picture: '',
            file: '',
            data: '',
            isLoading: false,
            selectedOne: '',
            location: ''
        }
    }
    confirm = () => {
        Alert.alert(
          'Confirmation',
          'post your story ...',
          [

            {text: 'Yes', onPress: () => this.postDataStory()},
            {text: 'No', onPress: () => this.props.navigation.pop(), style: 'cancel'},
          ]
        );
      }
    postDataStory = async () => {
        this.setState({ isLoading: true })
        try {
            const token = await AsyncStorage.getItem('@token')
            const postStory = async (bodyParameter) => await Axios.post('https://glints-redu.herokuapp.com/api/stories', bodyParameter, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `bearer ${token}`
                }
            })
            postStory({
                title: this.state.title,
                body: this.state.body,
                category: this.state.category,
                location: this.state.location
            })
                .then(res => {
                    this.setState({
                        id: res.data.data._id
                    })
                    this.postPhotoStory()
                })
                .catch(err => {
                    this.setState({ isLoading: false })
                    ToastAndroid.show('Please fill all the field', ToastAndroid.SHORT)
                })
        } catch (e) {
        }
    }

    handleChoosePhoto = () => {
        const options = {
            noData: true,
            title: 'Choose Image',
            storageOptions: {
                skipBackup: true,
                path: 'images'
            },
        };


        ImagePicker.showImagePicker(options, (response) => {
            this.setState({
                file: response,
                picture: response.uri
            })
            if (response.didCancel) {
                ToastAndroid.show('Cancel'.ToastAndroid.SHORT)
            } else if (response.error) {
                ToastAndroid.show('Error'.ToastAndroid.SHORT)
            } else {
                this.setState({
                    file: response,
                    picture: response.uri
                })
            }
        });
    }

    postPhotoStory = async () => {
        this.setState({ isLoading: true })
        try {
            const token = await AsyncStorage.getItem('@token')
            let file = new FormData();
            file.append('file', {
                uri: this.state.file.uri,
                name: this.state.file.fileName,
                type: 'image/jpeg'
            })
            const postPhoto = async (data) => await Axios.put(`https://glints-redu.herokuapp.com/api/stories/pict/${this.state.id}`, data, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `bearer ${token}`
                }
            })
            postPhoto(file)
                .then(res => {
                    ToastAndroid.show('Success', ToastAndroid.SHORT)
                    this.props.navigation.navigate('Home')
                })
                .catch(err => {
                    this.setState({ isLoading: false })
                })
        } catch (e) {
        }
    }

    getUserData = async () => {
        try {
            const token = await AsyncStorage.getItem('@token')
            const getUser = async () => await Axios.get('https://glints-redu.herokuapp.com/api/users/me', {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `bearer ${token}`
                }
            })
            getUser()
                .then(res => {
                    this.setState({
                        data: res.data.data.photo
                    })
                })
        } catch (e) { }
    }

    componentDidMount() {
        this.getUserData()
    }

    onValueChange = (value) => {
        this.setState({
            category: value
        });
    };

    render() {
        return (
            <Container style={{ backgroundColor: '#ffffff' }}>

                <Header transparent
                    style={{
                        backgroundColor: '#015249',
                        marginTop: -25
                    }}>
                    <Left style={{ flex: 1 }}>
                        <Icon
                            type="MaterialIcons"
                            name="arrow-back"
                            style={{
                                color: '#ffffff'
                            }}
                            onPress={
                                () => {
                                    this.props.navigation.pop()
                                }
                            }
                        />
                    </Left>

                    <Body style={{ flex: 1 }}>
                        <Title style={{ fontWeight: 'bold', fontFamily: 'serif' }}>
                            <Text style={{ fontSize: 21, color: '#22cade' }}>N</Text>
                            <Text style={{ fontSize: 20, color: '#ffffff' }}>ew</Text>
                            <Text style={{ fontSize: 21, color: '#22cade' }}> P</Text>
                            <Text style={{ fontSize: 20, color: '#ffffff' }}>ost</Text>
                        </Title>
                    </Body>

                    <Right style={{ flex: 1 }}>

                    </Right>
                </Header>



                <ScrollView>



                    <View
                        style={{
                            alignItems: 'center',
                            justifyContent: 'center',
                            flex: 0.25
                        }}
                    >

                        <Item
                            fixedLabel
                            style={{
                                width: Dimensions.get('window').width * 5 / 5,
                                borderColor: '#a5a5af'
                            }}
                        >
                            <Image
                                style={{
                                    width: 45,
                                    height: 45,
                                    borderRadius: 45 / 2,
                                    marginHorizontal: 20,
                                    marginVertical: 20
                                }}
                                source={{ uri: this.state.data.secure_url ? this.state.data.secure_url : null }}
                            />

                            <Input
                                placeholder='Title...'
                                placeholderTextColor='#a5a5af'
                                value={this.state.title}
                                multiline={true}
                                onChangeText={(text) => this.setState({ title: text })}
                                style={{ color: '#000000' }}
                            />

                        </Item>

                        <Item>
                            <Input
                                placeholder='Location...'
                                placeholderTextColor='#a5a5af'
                                value={this.state.location}
                                onChangeText={(text2) => this.setState({ location: text2 })}
                                style={{ color: '#000000', marginHorizontal: 20, marginVertical: 20 }}
                            />
                        </Item>
                        <Item
                            fixedLabel
                            style={{
                                width: Dimensions.get('window').width * 5 / 5,
                                borderColor: '#a5a5af',
                            }}
                        >
                            <Input
                                placeholder='Your Story ...'
                                placeholderTextColor='#a5a5af'
                                value={this.state.body}
                                multiline={true}
                                onChangeText={(text) => this.setState({ body: text })}
                                style={{ color: '#000000', height: 120, marginHorizontal: 20, marginVertical: 20 }}
                            />
                            <Image
                                style={{
                                    width: 100,
                                    height: 100,
                                    marginHorizontal: 20
                                }}
                                source={{ uri: this.state.picture ? this.state.picture : null }}
                            />
                        </Item>

                    </View>

                    <View
                        style={{
                            justifyContent: 'center',
                            marginHorizontal: 20,
                            flexDirection: 'column',
                            marginTop: 20
                        }}
                    >
                        <Text style={{ marginLeft: 8, fontSize: 16 }}>Category :</Text>
                        <Picker
                            mode="dropdown"
                            ioHeader="Select your Interest"
                            iosIcon={<Icon name="arrow-down" />}
                            selectedValue={this.state.category}
                            onValueChange={this.onValueChange.bind(this)}

                        >
                            <Picker.Item label="Nature" value="Nature" />
                            <Picker.Item label="City" value="City" />
                            <Picker.Item label="Religious" value="Religious" />
                            <Picker.Item label="Historical" value="Historical" />
                            <Picker.Item label="Cultural" value="Cultural" />
                            <Picker.Item label="Culinary" value="Culinary" />
                            <Picker.Item label="Tourism" value="Tourism" />


                        </Picker>

                    </View>

                    <View
                        style={{
                            justifyContent: 'space-evenly',
                            alignItems: 'center', display: 'flex',

                            flexDirection: 'row'
                        }}
                    >


                        <Button
                            onPress={this.handleChoosePhoto}
                            style={{
                                width: Dimensions.get('window').width * 0.4,
                                justifyContent: 'center',
                                backgroundColor: '#015249',
                                marginHorizontal: 20
                            }}
                        >

                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <Icon name="image" style={{ color: '#ffffff' }} />
                            </View>


                        </Button>

                        <Button
                            onPress={() => this.confirm()}
                            style={{
                                width: Dimensions.get('window').width * 0.4,
                                justifyContent: 'center',
                                backgroundColor: '#015249',
                                marginHorizontal: 20
                            }}
                        >
                            {
                                this.state.isLoading ?
                                    (<View>
                                        <ActivityIndicator animating={this.state.isLoading} size="small" color="#ffffff" />
                                    </View>)
                                    :
                            (
                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <Icon name="done-all" style={{ color: '#ffffff' }} />
                            </View>
                            ) 
                            }
                        </Button>
                    </View>

                </ScrollView>
                
                

            </Container>


        )
    }
}
export default withNavigation(AddStory)