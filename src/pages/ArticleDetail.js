import React, { Component } from 'react'
import { Text, View, Image, Dimensions, AsyncStorage, ToastAndroid, Alert } from 'react-native'
import { Container, Spinner, Left, Right, Icon, Button, Item, Content, Card, CardItem, Label, Input } from 'native-base';
import { withNavigation } from 'react-navigation';
import Axios from 'axios'
import Header2 from '../component/Header2';
class ArticleDetail extends Component {
    constructor(props) {
        super(props)
        this.state = {
            id: '',
            title: '',
            photo: '',
            date: '',
            category: '',
            body: '',
            isLoading: false,
            location: '',
            newComment: '',
            comment: []
        }
    }

    componentDidMount() {
        this.getArticleById()
    }

    PostCommentArticle = async (id) => {
        try {
            const token = await AsyncStorage.getItem('@token')
            const PostComment = async (bodyParameter) => await Axios.post(`https://glints-redu.herokuapp.com/api/comments?content=${id}`, bodyParameter, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `bearer ${token}`
                }
            })
            PostComment({
                text: this.state.newComment
            })
                .then(res => {
                    this.getArticleById()
                    this.setState({
                        newComment: ''
                    })
                })
                .catch(err => {
                })
        } catch (e) {
        }
    }

    confirm = () => {
        Alert.alert(
            'Confirmation',
            'Sign Out from your account',
            [

                { text: 'Yes', onPress: () => this.PostCommentArticle(this.state.id) },
                { text: 'No', onPress: () => ToastAndroid.show('Canceled', ToastAndroid.SHORT), style: 'cancel' },
            ]
        );
    }

    getArticleById = async () => {
        this.setState({ isLoading: true })
        const id = await this.props.navigation.getParam('id', '');
        try {
            const getArticle = async () => await Axios.get(`https://glints-redu.herokuapp.com/api/contents/detail/${id}`, {
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            getArticle()
                .then(res => {
                    this.setState({
                        title: res.data.content.title,
                        photo: res.data.content.photo,
                        date: res.data.content.date,
                        category: res.data.content.category,
                        body: res.data.content.body,
                        location: res.data.content.location,
                        comment: res.data.comment,
                        id: res.data.content._id,
                        isLoading: false
                    })
                })
                .catch(err => {
                    this.setState({ isLoading: false })
                })
        } catch (e) {
        }
    }

    render() {
        const newDate = new Date(this.state.date);
        const dateDay = newDate.toDateString().substring(0, 3);
        const dateDayDate = newDate.toDateString().substring(8, 10);
        const dateMonth = newDate.toDateString().substring(4, 7);
        const dateYear = newDate.toDateString().substring(11, 15);
        const upWord = this.state.category.toUpperCase().substring(0, 1)
        const downWord = this.state.category.substring(1, 7)

        const Maps = this.state.comment.map(item => {
            const newDate = new Date(item.date);
            const dateDay = newDate.toDateString().substring(0, 3);
            const dateDayDate = newDate.toDateString().substring(8, 10);
            const dateMonth = newDate.toDateString().substring(4, 7);
            return (
                <View
                    key={item._id}
                    style={{ alignItems: 'center', justifyContent: 'center' }}>
                    <Card
                        style={{
                            width: Dimensions.get('window').width * 0.90,
                            borderRadius: 10
                        }}>
                        <CardItem
                            header
                            button
                            onPress={() => { this.props.navigation.navigate('UserDataById', { id: item._user._id }) }}
                            style={{ borderRadius: 200 / 12 }}>
                            <Left>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Image
                                        style={{
                                            width: 25,
                                            height: 25,
                                            borderRadius: 25 / 2
                                        }}
                                        source={{ uri: item._user.photo.secure_url }}
                                    />
                                    <Text
                                        style={{
                                            fontSize: 15,
                                            fontFamily: 'Roboto',
                                            fontWeight: 'bold',
                                        }}
                                    > {item._user.name} </Text>
                                </View>
                            </Left>
                        </CardItem>
                        <CardItem
                            body
                            style={{
                                borderRadius: 200 / 12,
                                marginTop: -10
                            }}>
                            <Label
                                style={{
                                    fontSize: 15,
                                    marginTop: -10,
                                    marginLeft: 20,
                                    fontFamily: 'Roboto',
                                }}
                            > {item.text} </Label>
                        </CardItem>
                        <CardItem
                            footer
                            style={{
                                borderRadius: 200 / 12,
                                marginTop: -12
                            }}>
                            <Right>
                                <Text
                                    style={{
                                        fontSize: 10,
                                        marginTop: -10,
                                        fontFamily: 'Roboto',
                                        marginRight: Dimensions.get('window').width * -0.18
                                    }}>
                                    Posted: {dateDay + ', ' + dateDayDate + ' ' + dateMonth + ' ' + dateYear}
                                </Text>
                            </Right>
                        </CardItem>
                    </Card>
                </View>
            )
        })
        return (
            <Container>
                <Header2 />
                <Content>
                    {
                        this.state.isLoading ?

                            (<Spinner />)
                            :
                            (<View
                                style={{
                                    marginHorizontal: 20
                                }}
                            >
                                <Item><Text
                                    style={{
                                        fontSize: 20,
                                        fontWeight: 'bold',
                                        marginVertical: 10,
                                        fontFamily: 'Roboto',
                                        color: '#015249'
                                    }}
                                >{this.state.title}</Text>
                                </Item>
                                <Item><Image
                                    style={{
                                        width: '100%',
                                        height: 300,
                                        marginVertical: 10,
                                        borderRadius: 200 / 15
                                    }}
                                    source={{ uri: this.state.photo ? this.state.photo.secure_url : null }}
                                />
                                </Item>
                                <Item style={{
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}>
                                    <Icon
                                        type='Entypo'
                                        name='location-pin'
                                        style={{
                                            color: '#015249'
                                        }}
                                    />
                                    <Text
                                        style={{
                                            fontSize: 20,
                                            fontWeight: 'bold',
                                            marginVertical: 10,
                                            fontFamily: 'Roboto',
                                            color: '#015249'
                                        }}
                                    >{this.state.location}</Text>
                                </Item>
                                <Item>
                                    <Text
                                        style={{
                                            marginVertical: 5
                                        }}
                                    >{this.state.body}</Text></Item>
                                <Item >
                                    <Left><Text>category: </Text></Left>
                                    <Right><Text>{upWord + downWord}</Text></Right>
                                </Item>
                                <Item>
                                    <Left><Text>Posted: </Text></Left>
                                    <Right><Text>{dateDay + ', ' + dateDayDate + ' ' + dateMonth + ' ' + dateYear}</Text></Right>
                                </Item>
                                <Item
                                    style={{
                                        marginVertical: 20,
                                        flexDirection: 'column',
                                        alignItems: 'flex-start'
                                    }}
                                >
                                    <Label>Comments</Label>
                                    {Maps}
                                </Item>
                                <Card
                                    style={{
                                        width: Dimensions.get('window').width * 0.90,
                                        borderRadius: 10
                                    }}>
                                    <Item>
                                        <Left>
                                            <Input
                                                style={{
                                                    width: Dimensions.get('window').width * 0.70,
                                                }}
                                                placeholder='New comment'
                                                value={this.state.newComment}
                                                onChangeText={(text) => this.setState({ newComment: text })}
                                            />
                                        </Left>
                                        <Right>
                                            <Button
                                                transparent
                                                style={{ marginRight: 20 }}
                                                onPress={
                                                    () => {
                                                        switch (true) {
                                                            case this.state.newComment === '':
                                                                ToastAndroid.show('please type your comment', ToastAndroid.SHORT);
                                                                break;
                                                            default: this.confirm()
                                                        }
                                                    }}
                                            >

                                                <Label
                                                    style={{
                                                        color: '#22cade'
                                                    }}>
                                                    Post
                                                </Label>
                                            </Button>
                                        </Right>
                                    </Item>

                                </Card>
                            </View>)
                    }
                </Content>
            </Container>
        )
    }
}
export default withNavigation(ArticleDetail)