import React, { Component } from 'react'
import { Text, View, Image, FlatList, Dimensions, ActivityIndicator, StyleSheet } from 'react-native'
import { Container, CardItem, Card, Left, Fab, Button, Icon, Header, Right, Body, Title } from 'native-base'
import { withNavigation } from 'react-navigation'
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

class ArticleList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            isLogin: '',
            page: 1,
            refreshing: false,
            loading: false,
            category: '',
            isHidden: false
        }
    }

    getDataArticle = () => {
        try {
            const getArticle = async () => await Axios.get(`https://glints-redu.herokuapp.com/api/contents/paginate?page=${this.state.page}`, {
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            this.setState({ loading: true });

            getArticle()
                .then(res => {
                    this.setState({
                        data: [...this.state.data, ...res.data.data.docs],
                        refreshing: false,
                        loading: false
                    })
                })
                .catch(err => {
                })
        } catch (e) { }
    }

    renderFooter = () => {
        if (!this.state.loading) return null;

        return (
            <View
                style={{
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    borderColor: "#CED0CE"
                }}
            >
                <ActivityIndicator animating size="large" />
            </View>
        );
    }

    componentDidMount() {
        this.getDataArticle()
        this.userIsSignIn()
        setInterval(this.userIsSignIn, 1000)
    }

    handleEnd = () => {
        this.setState(state => ({ page: state.page + 1 }), () => this.getDataArticle());
    };

    userIsSignIn = async () => {
        const data = await AsyncStorage.getItem('@token')
        this.setState({ isLogin: data })
    }

    handleRefresh = () => {
        this.setState(
            {
                page: 1,
                refreshing: true,
                data: []
            },
            () => {
                this.getDataArticle();
            }
        );
    };

    render() {
        return (
            <Container style={{ backgroundColor: '#faffff' }}>
                <FlatList
                    data={this.state.data}
                    renderItem={({ item }) => (
                        <View
                            key={item._id}
                            style={{ alignItems: 'center', justifyContent: 'center' }}>
                            <Card
                                style={{
                                    marginVertical: 10,
                                    borderRadius: 200 / 12,
                                    width: Dimensions.get('window').width * 0.95,
                                }}>


                                <CardItem
                                    button
                                    onPress={() => this.props.navigation.navigate('ArticleDetail', { id: item._id })}
                                    style={{ borderRadius: 200 / 12 }}>
                                    <Text
                                        style={{
                                            fontSize: 18,
                                            fontFamily: 'Roboto',
                                            fontWeight: 'bold',
                                            color: '#015249'
                                        }}
                                    > {item.title} </Text>
                                </CardItem>


                                <CardItem
                                    cardBody
                                    button
                                    onPress={() => this.props.navigation.navigate('ArticleDetail', { id: item._id, photo: item._user.photo.secure_url })}
                                    style={{ marginHorizontal: 10 }}
                                >
                                    <Image
                                        style={{
                                            width: '100%',
                                            height: 300,
                                            borderRadius: 200 / 12,
                                        }}
                                        source={{ uri: item.photo ? item.photo.secure_url : null }}
                                    />
                                </CardItem>
                            </Card>
                        </View>
                    )}
                    keyExtractor={item => item._id}
                    onEndReached={this.handleEnd}
                    onEndReachedThreshold={1}
                    onRefresh={this.handleRefresh}
                    refreshing={this.state.refreshing}
                    // ListHeaderComponent={this.renderHeader}
                    ListFooterComponent={this.renderFooter}
                />
            </Container>

        )
    }
}
export default withNavigation(ArticleList)