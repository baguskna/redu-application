import React, { Component } from 'react'
import { Text, View, ScrollView, Image, Dimensions, ActivityIndicator, } from 'react-native'
import { withNavigation, FlatList } from 'react-navigation';
import { Container, CardItem, Card, Header, Title, Left, Fab, Button, Body, Item, Input } from 'native-base';
import Axios from 'axios';
import Icon from 'react-native-ionicons';

class Community extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            search: '',
            loading: false,
            refreshing: false,
        }
    }
    getDataComunities = () => {
        try {
            getData = async () => await Axios.get('https://glints-redu.herokuapp.com/api/communities', {
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            this.setState({ loading: true });
            getData()
                .then(res => {
                    this.setState({
                        data: res.data.data,
                        refreshing: false,
                    })
                })
                .catch(err => {
                })
        } catch (e) { }
    }
    componentDidMount() {
        this.getDataComunities()
        setInterval(this.getDataComunities, 1000)
    }

    renderFooter = () => {
        if (!this.state.loading) return null;

        return (
            <View
                style={{
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    borderColor: "#CED0CE"
                }}
            >
                <ActivityIndicator animating size="large" />
            </View>
        );
    }
    searchLocation = async () => {
        this.setState({ isLoading: true })
        try {
            const searchLoc = async () => await Axios.get(`https://glints-redu.herokuapp.com/api/communities/filter/${this.state.search}`, {
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            searchLoc()
                .then(res => {
                    this.setState({
                        data: res.data.data,
                        isLoading: false
                    })
                })
                .catch(err => {
                    this.setState({
                        isLoading: false
                    })
                })
        } catch (e) { }
    }

    handleRefresh = () => {
        this.setState(
            {
                page: 1,
                refreshing: true,
                data: []
            },
            () => {
                this.getDataComunities();
            }
        );
    };
    render() {
        return (
            <Container>
                <FlatList
                    data={this.state.data}
                    renderItem={({ item }) => (
                        <View
                            key={item._id}
                            style={{ alignItems: 'center', justifyContent: 'center' }}>
                            <Card
                                style={{
                                    marginVertical: 10,
                                    borderRadius: 200 / 12,
                                    width: Dimensions.get('window').width * 0.95,
                                }}>


                                <CardItem
                                    button
                                    onPress={() => this.props.navigation.navigate('ArticleDetail', { id: item._id })}
                                    style={{ borderRadius: 200 / 12 }}>
                                    <Text
                                        style={{
                                            fontSize: 18,
                                            fontFamily: 'Roboto',
                                            fontWeight: 'bold',
                                            color: '#015249'
                                        }}
                                    > {item.name} </Text>
                                </CardItem>


                                <CardItem
                                    cardBody
                                    button
                                    onPress={() => this.props.navigation.navigate('CommunityDetail', { id: item._id })}
                                    style={{ marginHorizontal: 10 }}
                                >
                                    <Image
                                        style={{
                                            width: '100%',
                                            height: 300,
                                            borderRadius: 200 / 12,
                                        }}
                                        source={{ uri: item.photo ? item.photo.secure_url : null }}
                                    />
                                </CardItem>
                            </Card>
                        </View>
                    )}
                    keyExtractor={item => item._id}
                    onEndReached={this.handleEnd}
                    onEndReachedThreshold={0.01}
                    onRefresh={this.handleRefresh}
                    refreshing={this.state.refreshing}
                    // ListHeaderComponent={this.renderHeader}
                    ListFooterComponent={this.renderFooter}
                />
            </Container>
        )
    }
}
export default withNavigation(Community)