import React, { Component } from 'react'
import { Text, View, Image, Dimensions, ToastAndroid, Linking } from 'react-native'
import { withNavigation } from 'react-navigation'
import Axios from 'axios';
import { Container, Item, Right, Left, Body, Spinner } from 'native-base';
import Icon from 'react-native-ionicons';
import AsyncStorage from '@react-native-community/async-storage';
import Header2 from '../component/Header2';

class CommunityDetail extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      photo: '',
      isLoading: false,
    }
  }

  getCommunity = async () => {
    this.setState({ isLoading: true })
    try {
      const id = await this.props.navigation.getParam('id', '');
      const getData = async () => await Axios.get(`https://glints-redu.herokuapp.com/api/communities/detail/${id}`, {
        headers: {
          'Content-Type': 'application/json'
        }
      })
      getData()
        .then(res => {
          this.setState({
            data: res.data.data,
            photo: res.data.data.photo,
            isLoading: false
          })
          console.log(res.data.data)
        })
        .catch(err => {
          this.setState({ isLoading: false })
        })
    } catch (e) { }
  }

  componentDidMount() {
    this.getCommunity()
  }

  render() {
    return (
      <Container>

        <Header2 />

        {
          this.state.isLoading ?

            (<Spinner />)

            :

            (<View style={{ alignItems: 'center', justifyContent: 'center' }}>
              <Item>
                <Text
                  style={{
                    fontSize: 20,
                    fontWeight: 'bold',
                    marginVertical: 10,
                    fontFamily: 'Roboto',
                    color: '#015249',
                  }}
                >{this.state.data.name}</Text>
              </Item>
              <Item >
                <Image
                  style={{
                    width: Dimensions.get('window').width * 0.95,
                    height: 300,
                    borderRadius: 200 / 15,
                    marginHorizontal: 25
                  }}
                  source={{ uri: this.state.photo.secure_url ? this.state.photo.secure_url : null }} />
              </Item>
              <Item style={{
                justifyContent: 'center',
                alignItems: 'center',
              }}>
                <Icon
                  type='Entypo'
                  name='location-pin'
                  style={{
                    color: '#015249'
                  }}
                />
                <Text
                  style={{
                    fontSize: 20,
                    fontWeight: 'bold',
                    marginVertical: 10,
                    fontFamily: 'Roboto',
                    color: '#015249'
                  }}
                >{this.state.data.location}</Text>
              </Item>
              <Item>
                <Text
                  style={{
                    marginVertical: 5
                  }}
                >{this.state.data.description}</Text></Item>
              <Item>
                  <Text
                    style={{
                      color: 'blue'
                    }}
                    onPress={() => Linking.openURL(this.state.data.url)}
                  >Join Now</Text>
              </Item>

            </View>)

        }


      </Container>
    )
  }
}
export default withNavigation(CommunityDetail);
