import React, { Component } from 'react';
import { Text, ToastAndroid, Image, Dimensions } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Axios from 'axios';
import { Container, Content, CardItem, Card, Button, Body, Left, Right, View, Header, Title, Label } from 'native-base';
import { withNavigation, FlatList } from 'react-navigation';
import Icon from 'react-native-ionicons';

class EditStory extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      photo: ''
    };
  };

  getMyStory = async () => {
    try {
      const token = await AsyncStorage.getItem('@token')
      const getMy = async () => await Axios.get('https://glints-redu.herokuapp.com/api/stories/mine', {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `bearer ${token}`
        }
      })
      getMy()
        .then(res => {
          this.setState({
            data: res.data.data,
            photo: res.data.data.photo
          })
        })
        .catch(err => {
        })
    } catch (e) {
    }
  }

  componentDidMount() {
    this.getMyStory()
  }

  delStory = async (id) => {
    try {
      const token = await AsyncStorage.getItem('@token')
      const deleteItem = async (idDel) => await Axios.delete(`https://glints-redu.herokuapp.com/api/stories/delete/${idDel}`, {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `bearer ${token}`
        }
      })
      deleteItem(id)
        .then(res => {
          ToastAndroid.show(res.data.message, ToastAndroid.SHORT)
          this.getMyStory()
        })
        .catch(err => {
          ToastAndroid.show('Failed to Delete', ToastAndroid.SHORT)
        })
    } catch (e) {
    }
  }

  render() {
    return (
      <Container>
        <Header transparent
          style={{
            backgroundColor: '#015249',
            marginTop: -25
          }}>
          <Left style={{ flex: 1 }}>
            <Icon
              type="MaterialIcons"
              name="arrow-back"
              style={{
                color: '#ffffff'
              }}
              onPress={
                () => {
                  this.props.navigation.pop()
                }
              }
            />
          </Left>

          <Body style={{ flex: 1 }}>
            <Title style={{ fontFamily: 'Roboto', fontWeight: 'bold' }}>
              <Text style={{ fontWeight: 'bold', fontFamily: 'serif' }}>
                <Text style={{ fontSize: 22, color: '#22cade' }}>M</Text>
                <Text style={{ fontSize: 15, color: '#ffffff' }}>Y</Text>
                <Text style={{ fontSize: 22, color: '#22cade' }}> S</Text>
                <Text style={{ fontSize: 15, color: '#ffffff' }}>TORY</Text>
              </Text>
            </Title>
          </Body>

          <Right style={{ flex: 1 }}>

          </Right>
        </Header>
        <FlatList
          data={this.state.data}
          renderItem={({ item }) => (
            <View
              key={item._id}
              style={{ alignItems: 'center', justifyContent: 'center' }}>
              <Card
                style={{
                  marginVertical: 10,
                  borderRadius: 200 / 12,
                  width: Dimensions.get('window').width * 0.95,
                }}>


                <CardItem
                  button
                  onPress={() => this.props.navigation.navigate('EditStoryDetail', { id: item._id })}
                  style={{ borderRadius: 200 / 12 }}>
                  <Text
                    style={{
                      fontSize: 18,
                      fontFamily: 'Roboto',
                      fontWeight: 'bold',
                      color: '#015249'
                    }}
                  > {item.title} </Text>
                </CardItem>


                <CardItem
                  cardBody
                  button
                  onPress={() => console.log(item.isPublished)}
                  style={{ marginHorizontal: 10, marginVertical: 10 }}
                >
                  <Image
                    style={{
                      width: '100%',
                      height: 300,
                      borderRadius: 200 / 12,
                    }}
                    source={{ uri: item.photo ? item.photo.secure_url : null }}
                  />
                </CardItem>
                <CardItem>
                  <Left>
                  <Label>

                  Published Status :
                  </Label>
                  </Left>
                  <Right>
                    {item.isPublished ? (<Label>Published</Label>) :
                      (<Label>Not Published</Label>)}
                  </Right>
                </CardItem>
              </Card>
            </View>
          )}
          keyExtractor={item => item._id}
          onEndReached={this.handleEnd}
          onEndReachedThreshold={1}
          onRefresh={this.handleRefresh}
          refreshing={this.state.refreshing}
        />
      </Container>

    )
  }
}
export default withNavigation(EditStory);