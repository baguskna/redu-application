import React, { Component } from 'react'
import { Container, Tabs, Tab } from 'native-base'
import { withNavigation } from 'react-navigation'
import Header1 from '../component/Header1';
import Story from './Story';
import ArticleList from './ArticleList';
import Community from './Community';

class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }

    render() {
        return (
            <Container style={{ backgroundColor: '#faffff' }}>
                <Header1 />
                <Tabs
                    tabContainerStyle={{ height: 40 }}
                    tabBarUnderlineStyle={{ backgroundColor: '#22cade', height: 5 }}>
                    <Tab heading="Story" tabStyle={{ backgroundColor: '#ffffff' }} textStyle={{ color: '#a3a3a3' }} activeTabStyle={{ backgroundColor: '#ffffff' }} activeTextStyle={{ color: '#22cade', fontWeight: 'normal' }}>
                        <Story />
                    </Tab>
                    <Tab heading="Article" tabStyle={{ backgroundColor: '#ffffff' }} textStyle={{ color: '#a3a3a3' }} activeTabStyle={{ backgroundColor: '#ffffff' }} activeTextStyle={{ color: '#22cade', fontWeight: 'normal' }}>
                        <ArticleList />
                    </Tab>
                    <Tab heading="Community" tabStyle={{ backgroundColor: '#ffffff' }} textStyle={{ color: '#a3a3a3' }} activeTabStyle={{ backgroundColor: '#ffffff' }} activeTextStyle={{ color: '#22cade', fontWeight: 'normal' }}>
                        <Community />
                    </Tab>
                </Tabs>
            </Container>

        )
    }
}
export default withNavigation(Home)