import React, { Component } from 'react'
import { Text, View, Image, FlatList, Dimensions } from 'react-native'
import { Container, CardItem, Card, Header, Title, Left, Fab, Button, Icon } from 'native-base'
import { withNavigation } from 'react-navigation'
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import Header5 from '../component/Header5';

class KontenByCategory extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            isLogin: '',
            refreshing: false,
            loading: false,
            category:''
        }
    }

    getDataArticle = async () => {
        try {
            
            const category = await this.props.navigation.getParam('category', '');
            const getArticle = async () => await Axios.get(`https://glints-redu.herokuapp.com/api/contents/search?category=${category}`, {
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            this.setState({ loading: true });

            getArticle()
                .then(res => {
                    this.setState({
                        data: [...this.state.data, ...res.data.data.docs],
                        refreshing: false,
                        loading: false
                    })
                })
                .catch(err => {
                })
        } catch (e) { }
    }

    renderFooter = () => {
        if (!this.state.loading) return null;

        return (
            <View
                style={{
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    borderColor: "#CED0CE"
                }}
            >
                <ActivityIndicator animating size="large" />
            </View>
        );
    }

    componentDidMount() {
        this.getDataArticle()
    }

    handleRefresh = () => {
        this.setState(
            {
                refreshing: true,
                data: []
            },
            () => {
                this.getDataArticle();
            }
        );
    };

    render() {
            return (<Container style={{ backgroundColor: '#f0efed' }}>

               <Header5/>
                <FlatList
                    data={this.state.data}
                    renderItem={({ item }) => (
                        <View
                            key={item._id}
                            style={{ alignItems: 'center', justifyContent: 'center' }}>
                            <Card
                                style={{
                                    marginVertical: 10,
                                    borderRadius: 200 / 12,
                                    width: Dimensions.get('window').width * 0.95,
                                }}>


                                <CardItem
                                    button
                                    onPress={() => this.props.navigation.navigate('ArticleDetail', { id: item._id })}
                                    style={{ borderRadius: 200 / 12 }}>
                                    <Text
                                        style={{
                                            fontSize: 18,
                                            fontFamily: 'Roboto',
                                            fontWeight: 'bold',
                                            color: '#015249'
                                        }}
                                    > {item.title} </Text>
                                </CardItem>


                                <CardItem
                                    cardBody
                                    button
                                    onPress={() => this.props.navigation.navigate('ArticleDetail', { id: item._id })}
                                    style={{ marginHorizontal: 10 }}
                                >
                                    <Image
                                        style={{
                                            width: '100%',
                                            height: 300,
                                            borderRadius: 200 / 12,
                                        }}
                                        source={{ uri: item.photo ? item.photo.secure_url : null }}
                                    />
                                </CardItem>

                            </Card>
                        </View>
                    )}
                    keyExtractor={item => item._id}
                    onEndReached={this.handleEnd}
                    onEndReachedThreshold={50}
                    onRefresh={this.handleRefresh}
                    refreshing={this.state.refreshing}
                />
            </Container>)
    }
}
export default withNavigation(KontenByCategory)