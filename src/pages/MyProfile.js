import React, { Component } from 'react';
import { Image, View, Dimensions, ScrollView, Alert, ToastAndroid, Text } from 'react-native';
import { withNavigation } from 'react-navigation';
import { Container, Content, Card, Header, Title, Left, Right, Spinner, Item, Button, Label } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';

class MyProfile extends Component {
  constructor(props) {
    super(props)
    this.state = {
      picture: '',
      email: '',
      name: '',
      data: '',
      interest: []
    }
  }

  fetchDataProfile = () => {
    AsyncStorage.getItem('profile')
      .then(res => {
        let profileData = JSON.parse(res)
        this.setState({
          data: profileData,
          name: profileData.name,
          email: profileData.email,
          picture: profileData.photo.secure_url,
          interest: profileData.interest
        })
      })
  }

  componentDidMount() {
    this.fetchDataProfile()
  }

  render() {
    const Maps = this.state.interest.map((item, i) => {
      return (
        <Label
          style={{
            fontSize: 20,
            marginLeft: 20,
            fontFamily: 'Roboto',
          }}
        >{item}</Label>
      )
    })
    return (
      <Container style={{ backgroundColor: '#f5f5f5' }}>


        <Header style={{ backgroundColor: '#015249' }}>
          <Title style={{ fontWeight: 'bold', fontFamily: 'serif' }}>
            <Text style={{ fontSize: 30, color: '#22cade' }}>P</Text>
            <Text style={{ fontSize: 20, color: '#ffffff' }}>ROFILE</Text>
          </Title>
        </Header>

        <Content>
          <View
            style={{
              flexDirection: 'column',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              marginVertical: 35
            }}
          >

            <Image
              style={{
                width: 200,
                height: 200,
                borderRadius: 200 / 2,
              }}
              source={{ uri: this.state.picture ? this.state.picture : null }}
            />

          </View>
          <View style={{ alignItems: 'center', justifyContent: 'center' }}>
            <Item
              style={{
                width: Dimensions.get('window').width * 4.5 / 5,
                borderColor: '#000000'
              }}
            />
            <Item
              fixedLabel
              style={{
                width: Dimensions.get('window').width * 4.5 / 5,
                borderColor: '#000000',
                flexDirection: 'column',
                alignItems: 'flex-start',
                marginVertical: 10
              }}
            >
              <Label
                style={{
                  fontSize: 20,
                  fontWeight: 'bold',
                  marginLeft: 5,
                  fontFamily: 'Roboto',

                  color: '#000000'
                }}>
                Name :
                </Label>

              <Label
                style={{
                  fontSize: 20,
                  marginLeft: 20,
                  fontFamily: 'Roboto',
                }}>
                {this.state.data.name}
              </Label>
            </Item>
            <Item
              fixedLabel
              style={{
                width: Dimensions.get('window').width * 4.5 / 5,
                borderColor: '#000000',
                flexDirection: 'column',
                alignItems: 'flex-start',
                marginVertical: 10
              }}
            >
              <Label
                style={{
                  fontSize: 20,
                  fontWeight: 'bold',
                  marginLeft: 5,
                  fontFamily: 'Roboto',

                  color: '#000000'
                }}>
                Email :
                </Label>

              <Label
                style={{
                  fontSize: 20,
                  marginLeft: 20,
                  fontFamily: 'Roboto',
                }}>
                {this.state.data.email}
              </Label>
            </Item>
            <Item
              style={{
                width: Dimensions.get('window').width * 4.5 / 5,
                borderColor: '#000000',
                flexDirection: 'column',
                alignItems: 'flex-start',
                // marginVertical:
              }}>
              <Label
                style={{
                  fontSize: 20,
                  fontWeight: 'bold',
                  marginLeft: 5,
                  fontFamily: 'Roboto',

                  color: '#000000'
                }}>
                Interest :
                </Label>
              {Maps}
            </Item>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                marginVertical: 20
              }}
            >
              <Button
                style={{
                  backgroundColor: '#22cade',
                  alignItems: 'center',
                  justifyContent: 'center',
                  width: Dimensions.get('window').width * 4.5 / 5,
                  borderRadius: 200 / 18
                }}
                onPress={() => this.props.navigation.navigate('UpdateProfile')}
              >
                <Text
                      style={{
                        color: '#ffffff',
                        fontWeight: 'bold',
                        fontSize: 17
                      }}
                    > Edit </Text>
                
              </Button>
            </View>
          </View>

        </Content>

      </Container>

    )
  }
}

export default withNavigation(MyProfile);