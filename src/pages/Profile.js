import React, { Component } from 'react'
import { withNavigation } from 'react-navigation';
import ProfileContent from './ProfileContent';
import ProfileSignIn from './ProfileSignIn';
import AsyncStorage from '@react-native-community/async-storage';
import { Spinner, Container } from 'native-base';
import { connect } from 'react-redux';
import { getUser } from '../redux/action/user';

class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            token: 'null',
            data: '',
            timePassed: false

        }
    }

    setTimePassed = () => {
        this.setState({ timePassed: true });
    }

    storeToken = async () => {
        const data = await AsyncStorage.getItem('@token')
        this.setState({
            token: data
        })
    }

    componentDidMount() {
        this.storeToken()
        this.props.get_User(this.props.auth.token)
        setInterval(this.storeToken, 1000)
        setTimeout(() => {
            this.setTimePassed();
        }, 500);
    }

    render() {

        if (!this.state.timePassed) {
            return <Spinner />;
        } else {
            if (!(this.state.token == 'null' || this.state.token == null)) {
                return <ProfileContent />
                // } else if (this.state.token !== null) {
                //     return <ProfileSignIn />
            } else {
                return (
                    <Container>
                        <ProfileSignIn />
                    </Container>)
            }
        }
    }
}
const mapStateToProps = state => ({
    auth: state.auth,
    user: state.user
});

const mapDispatchToProps = dispatch => {
    return {
        get_User: token => dispatch(getUser(token))
    };
};
export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(Profile));