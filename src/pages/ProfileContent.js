import React, { Component } from 'react';
import { Text, Image, View, Dimensions, ScrollView, Alert, ToastAndroid } from 'react-native';
import { withNavigation, NavigationActions } from 'react-navigation';
import { Container, Content, CardItem, Card, Header, Title, Left, Right, Spinner } from 'native-base';
import { connect } from 'react-redux';
import { getUser } from '../redux/action/user';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-ionicons';

class ProfileContent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      picture: '',
      email: '',
      name: '',
      dataProfile: '',
    }
  }

  componentDidMount() {
    this.props.get_User(this.props.auth.token)
  }

  confirm = () => {
    Alert.alert(
      'Confirmation',
      'Sign Out from your account',
      [

        { text: 'Yes', onPress: () => this.delToken() },
        { text: 'No', onPress: () => ToastAndroid.show('Canceled', ToastAndroid.SHORT), style: 'cancel' },
      ]
    );
  }

  delToken = () => {
    AsyncStorage.setItem('@token', 'null')
    AsyncStorage.setItem('profile', 'null')
    Alert.alert('', 'Signed Out')
    this.props.navigation.popToTop()

  };
  render() {
    return (

      <ScrollView>

        <Container style={{ backgroundColor: '#e6e5e3' }}>


          <Header style={{ backgroundColor: '#015249' }}>
            <Title style={{ fontWeight: 'bold', fontFamily: 'serif' }}>
              <Text style={{ fontSize: 30, color: '#22cade' }}>P</Text>
              <Text style={{ fontSize: 20, color: '#ffffff' }}>ROFILE</Text>
            </Title>
          </Header>

          <Content>
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
              <Card
                style={{
                  marginVertical: 15,
                  height: 60,
                  justifyContent: 'center',
                  width: Dimensions.get('window').width * 0.95,
                  borderRadius: 200 / 18
                }}
              >
                <CardItem
                  button
                  onPress={() => this.props.navigation.navigate('MyProfile')}
                  style={{
                    borderRadius: 200 / 18
                  }}
                >
                  <Left>
                    <Icon style={{ color: '#bdbcbb' }} name="log-out" />
                    <Text style={{ marginLeft: 10, fontWeight: 'bold' }}>My Account</Text>
                  </Left>
                  <Right>
                    <Icon style={{ color: '#22cade' }} name="arrow-dropright" />
                  </Right>
                </CardItem>
              </Card>

              <Card
                style={{
                  marginVertical: 15,
                  height: 60,
                  justifyContent: 'center',
                  width: Dimensions.get('window').width * 0.95,
                  borderRadius: 200 / 18
                }}>
                <CardItem
                  button
                  onPress={() => this.props.navigation.navigate('EditStory')}
                  style={{
                    borderRadius: 200 / 18
                  }}
                >
                  <Left>
                    <Icon style={{ color: '#bdbcbb' }} name="card" />
                    <Text style={{ marginLeft: 10, fontWeight: 'bold' }}>My Story</Text>
                  </Left>
                  <Right>
                    <Icon style={{ color: '#22cade' }} name="arrow-dropright" />
                  </Right>
                </CardItem>
              </Card>

              <Card
                style={{
                  marginVertical: 15,
                  height: 60,
                  justifyContent: 'center',
                  width: Dimensions.get('window').width * 0.95,
                  borderRadius: 200 / 18
                }}
              >
                <CardItem
                  button
                  onPress={() => this.confirm()}
                  style={{
                    borderRadius: 200 / 18
                  }}
                >
                  <Left>
                    <Icon style={{ color: '#bdbcbb' }} name="power" />
                    <Text style={{ marginLeft: 10, fontWeight: 'bold' }}>Sign Out</Text>
                  </Left>
                  <Right>
                    <Icon style={{ color: '#22cade' }} name="arrow-dropright" />
                  </Right>
                </CardItem>
              </Card>

            </View>

          </Content>

        </Container>

      </ScrollView>
    )
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  user: state.user
});

const mapDispatchToProps = dispatch => {
  return {
    get_User: token => dispatch(getUser(token))
  };
};

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(ProfileContent));