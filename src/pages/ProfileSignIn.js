import React, { Component } from 'react';
import { Text, View, ImageBackground, Dimensions } from 'react-native';
import { Container, Button } from 'native-base';
import { withNavigation } from 'react-navigation';

class ProfileSignIn extends Component {
  render() {
    return (

      <Container style={{ backgroundColor: '#015249' }}>

        <ImageBackground
          source={require('../image/profilebackground.png')}
          style={{
            width: Dimensions.get('window').width,
            height: Dimensions.get('window').height,
            color: '#015249'
          }}
        >


          <View style={{ flexDirection: 'column' }}>

            <View
              style={{
                marginVertical: 80,
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <Text
                style={{
                  fontWeight: 'bold',
                  fontFamily: 'serif'
                }}
              >
                <Text style={{ fontSize: 48, color: '#22cade' }}>R</Text>
                <Text style={{ fontSize: 40, color: '#ffffff' }}>EDU</Text>
              </Text>
            </View>


            <View
              style={{
                justifyContent: 'space-evenly',
                alignItems: 'center', display: 'flex',
                marginVertical: 80,
                flexDirection: 'row'
              }}
            >

              <Button
                bordered
                rounded
                onPress={() => this.props.navigation.navigate('SignIn')}
                style={{
                  width: Dimensions.get('window').width * 0.4,
                  justifyContent: 'center',
                  borderColor: '#015249', 
                  marginVertical:10,
                  backgroundColor:'#ddfffb'
                }}
              >
                <Text
                  style={{
                    fontWeight: 'bold',
                    fontSize: 15,
                    color: '#015249'
                  }}
                > SIGN IN </Text>
              </Button>

              <Button
                bordered
                rounded
                onPress={() => this.props.navigation.navigate('SignUp')}
                style={{
                  width: Dimensions.get('window').width * 0.4,
                  justifyContent: 'center',
                  borderColor: '#015249',
                  backgroundColor:'#ddfffb'
                }}
              >
                <Text
                  style={{
                    color: '#015249',
                    fontSize: 15,
                    fontWeight: 'bold'
                  }}> SIGN UP </Text>
              </Button>

            </View>

          </View>

        </ImageBackground>

      </Container>
    )
  }
}

export default withNavigation(ProfileSignIn)