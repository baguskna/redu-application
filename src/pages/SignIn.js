import React, { Component } from 'react'
import { Text, View, Dimensions, ToastAndroid, ActivityIndicator } from 'react-native'
import { Container, Content, Item, Input, Button, Header, Left, Body, Title, Right, Icon } from 'native-base'
import { withNavigation } from 'react-navigation'
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import { auth } from '../redux/action/AuthAction';

class SignIn extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            isLoading: false,
            message: ''
        }
    }

    signIn = () => {
        this.setState({ isLoading: true });
        try {
            const signInRedu = async (bodyParameter) => await axios.post('https://glints-redu.herokuapp.com/api/users/login', bodyParameter,
                {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
            signInRedu({
                email: this.state.email,
                password: this.state.password
            })
                .then(res => {
                    this.props.FetchToken(res.data.data)
                    AsyncStorage.setItem('@token', res.data.data)
                    ToastAndroid.show("Sign In Success", ToastAndroid.SHORT)
                    this.props.navigation.popToTop()
                    this.setState({ isLoading: false })
                })
                .catch(() => {
                    this.setState({
                        isLoading: false,
                    })
                    ToastAndroid.show("email and password combination does not match", ToastAndroid.SHORT)
                })
        }
        catch{ }
    }


    render() {
        return (
            <Container style={{ backgroundColor: '#015249' }}>
                <Header
                    transparent
                    style={{
                        marginTop: -20
                    }}
                >
                    <Left
                        style={{
                            flex: 1
                        }}
                    >
                        <Icon
                            type="MaterialIcons"
                            name="arrow-back"
                            style={{
                                color: '#ffffff'
                            }}
                            onPress={
                                () => {
                                    this.props.navigation.pop()
                                }
                            }
                        />
                    </Left>
                    <Body style={{ flex: 1 }}>
                        <Title style={{ fontFamily: 'serif', fontWeight: 'bold' }}>
                            <Text style={{ fontSize: 30, color: '#22cade' }}>S</Text>
                            <Text style={{ fontSize: 20, color: '#ffffff' }}>IGN</Text>
                            <Text style={{ fontSize: 30, color: '#22cade' }}> I</Text>
                            <Text style={{ fontSize: 20, color: '#ffffff' }}>N</Text>
                        </Title>
                    </Body>
                    <Right style={{ flex: 1 }}>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.navigate('SignUp')}
                        >
                            <Text style={{ color: '#ffffff', fontWeight: 'bold' }}>SIGN UP</Text>
                        </Button>
                    </Right>
                </Header>


                <View
                    style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        flex: 0.5
                    }}
                >

                    <Item
                        fixedLabel
                        style={{
                            width: Dimensions.get('window').width * 4.5 / 5,
                            borderColor: '#5edfff'
                        }}
                    >
                        <Icon
                            type='Entypo'
                            name='mail'
                            style={{
                                color: '#ffffff'
                            }}
                        />
                        <Input
                            placeholder='Email'
                            placeholderTextColor='#ecfcff'
                            value={this.state.email}
                            onChangeText={(text) => this.setState({ email: text })}
                            keyboardType={'email-address'}
                            style={{ color: '#ffffff' }}
                        />
                    </Item>

                    <Item
                        fixedLabel
                        style={{
                            width: Dimensions.get('window').width * 4.5 / 5,
                            borderColor: '#5edfff'
                        }}
                    >
                        <Icon
                            type='Entypo'
                            name='lock'
                            style={{
                                color: '#ffffff'
                            }}
                        />
                        <Input
                            placeholder='Password'
                            placeholderTextColor='#ecfcff'
                            value={this.state.password}
                            onChangeText={(text) => this.setState({ password: text })}
                            secureTextEntry={true}
                            style={{ color: '#ffffff' }}
                        />
                    </Item>

                </View>


                <Content>

                    <View
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            flex: 4
                        }}
                    >

                        <Button
                            onPress={
                                () => {
                                    switch (true) {
                                        case this.state.email === '' && this.state.password === '':
                                            ToastAndroid.show('please fill email and password', ToastAndroid.SHORT);
                                            break;
                                        case this.state.email === '':
                                            ToastAndroid.show('please fill the email', ToastAndroid.SHORT);
                                            break;
                                        case this.state.password === '':
                                            ToastAndroid.show('please fill the password', ToastAndroid.SHORT);
                                            break;
                                        default: this.signIn()
                                    }
                                }}
                            style={{
                                backgroundColor: '#22cade',
                                alignItems: 'center',
                                justifyContent: 'center',
                                width: Dimensions.get('window').width * 4.5 / 5,
                                borderRadius: 200 / 18
                            }}
                        >
                            {
                                this.state.isLoading ?
                                    (<View>
                                        <ActivityIndicator animating={this.state.isLoading} size="large" color="#ffffff" />
                                    </View>)
                                    :
                                    (<Text
                                        style={{
                                            color: '#ffffff',
                                            fontWeight: 'bold',
                                            fontSize: 17
                                        }}
                                    > 
                                    SIGN IN </Text>)
                            }
                        </Button>
                    </View>
                </Content>
            </Container>
        )
    }
}

const mapStateToProps = state => ({
    auth: state.auth,
})

const mapDispatchToProps = dispatch => {
    return {
        FetchToken: (token) => dispatch(auth(token))
    }
};

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(SignIn));