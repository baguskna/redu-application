import React, { Component } from 'react';
import { Text, View, ToastAndroid, Dimensions, ActivityIndicator } from 'react-native';
import { withNavigation } from 'react-navigation';
import { Container, Content, Item, Input, Button, CheckBox, Body, Header, Left, Right, Icon, Title, List } from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';
import axios from 'axios';
import { switchCase } from '@babel/types';

class SignUp extends Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      email: '',
      password: '',
      password2: '',
      interest: [],
      checkboxOne: false,
      checkboxTwo: false,
      checkboxThree: false,
      checkboxFour: false,
      checkboxFive: false,
      checkboxSix: false,
      isLoading: false,
    }
  }

  signUp = () => {
    this.setState({ isLoading: true })
    try {
      const signUpRedu = async (bodyParameter) => await axios.post('https://glints-redu.herokuapp.com/api/users', bodyParameter,
        {
          headers: {
            'Content-Type': 'application/json'
          }
        })
      signUpRedu({
        name: this.state.name,
        email: this.state.email,
        password: this.state.password,
        interest: this.state.interest
      })
        .then(res => {
          ToastAndroid.show('User successfully saved', ToastAndroid.SHORT)
          this.props.navigation.navigate('SignIn')
        })
        .catch(err => {
          this.setState({ isLoading: false })
          ToastAndroid.show('Sign Up Failed', ToastAndroid.SHORT)
        })
    }
    catch (e) {
    }
  }

  componentDidMount() {
    this.setState({ isLoading: false })
  }

  render() {
    return (
      <Container style={{ backgroundColor: '#015249' }}>
        <Header
          transparent
          style={{
            marginTop: -20
          }}
        >
          <Left
            style={{
              flex: 1
            }}
          >
            <Icon
              type="MaterialIcons"
              name="arrow-back"
              style={{
                color: '#ffffff'
              }}
              onPress={
                () => {
                  this.props.navigation.pop()
                }
              }
            />
          </Left>
          <Body style={{ flex: 1 }}>
            <Title style={{ fontFamily: 'serif', fontWeight: 'bold' }}>
              <Text style={{ fontSize: 30, color: '#22cade' }}>S</Text>
              <Text style={{ fontSize: 20, color: '#ffffff' }}>IGN</Text>
              <Text style={{ fontSize: 30, color: '#22cade' }}> U</Text>
              <Text style={{ fontSize: 20, color: '#ffffff' }}>P</Text>
            </Title>
          </Body>
          <Right style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate('SignIn')}
            >
              <Text style={{ color: '#ffffff', fontWeight: 'bold' }}>SIGN IN</Text>
            </Button>
          </Right>
        </Header>
        <ScrollView>
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >

            <Item
              fixedLabel
              style={{
                width: Dimensions.get('window').width * 4.5 / 5,
                borderColor: '#5edfff'
              }}
            >
              <Input
                placeholder='Name'
                placeholderTextColor='#ecfcff'
                value={this.state.name}
                onChangeText={(text) => this.setState({ name: text })}
                style={{ color: '#ffffff' }}
              />
            </Item>

            <Item
              fixedLabel
              style={{
                width: Dimensions.get('window').width * 4.5 / 5,
                borderColor: '#5edfff'
              }}
            >
              <Input
                placeholder='Email'
                placeholderTextColor='#ecfcff'
                value={this.state.email}
                onChangeText={(text) => this.setState({ email: text })}
                keyboardType={'email-address'}
                style={{ color: '#ffffff' }}
              />
            </Item>

            <Item
              fixedLabel
              style={{
                width: Dimensions.get('window').width * 4.5 / 5,
                borderColor: '#5edfff'
              }}
            >
              <Input
                placeholder='Password'
                placeholderTextColor='#ecfcff'
                value={this.state.password}
                onChangeText={(text) => this.setState({ password: text })}
                secureTextEntry={true}
                style={{ color: '#ffffff' }}
              />
            </Item>
            <Item
              fixedLabel
              style={{
                width: Dimensions.get('window').width * 4.5 / 5,
                borderColor: '#5edfff'
              }}
            >
              <Input
                placeholder='Confirm password'
                placeholderTextColor='#ecfcff'
                value={this.state.password2}
                onChangeText={(text) => this.setState({ password2: text })}
                secureTextEntry={true}
                style={{ color: '#ffffff' }}
              />
            </Item>

          </View>


          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >


            <Item
              fixedLabel
              style={{
                width: Dimensions.get('window').width * 4.5 / 5,
                borderColor: '#5edfff'
              }}
            ><Text
              style={{
                marginVertical: 10,
                fontSize: 18,
                color: '#ecfcff',
              }}
            >Interest :</Text>
            </Item>
            <List
              style={{
                width: Dimensions.get('window').width * 4.25 / 5,
                justifyContent: 'center',
                alignItems: 'center',
                borderColor: '#5edfff',
                flexDirection: 'row',
                marginVertical: 5
              }}
            >
              <Left>
                <Text
                  style={{
                    color: '#ecfcff',
                    fontSize: 18,
                    marginLeft: 25
                  }}
                >Nature</Text>
              </Left>

              <Right>
                <CheckBox
                  checked={this.state.checkboxOne}
                  color='#22cade'
                  onPress={() => {
                    this.setState(state => {
                      if (state.interest.find(el => el == "Nature")) {
                        const index = state.interest.indexOf("Nature")
                        if (index > -1) {
                          state.interest.splice(index, 1)
                        }
                        return {
                          interest: state.interest,
                          checkboxOne: !this.state.checkboxOne
                        }
                      } else {
                        const list = state.interest.concat("Nature")
                        return {
                          interest: list,
                          checkboxOne: !this.state.checkboxOne
                        }
                      }
                    })
                  }}
                />
              </Right>

            </List>

            <List
              style={{
                width: Dimensions.get('window').width * 4.25 / 5,
                justifyContent: 'center',
                alignItems: 'center',
                borderColor: '#5edfff',
                flexDirection: 'row',
                marginVertical: 5
              }}
            >
              <Left>
                <Text
                  style={{
                    color: '#ecfcff',
                    fontSize: 18,
                    marginLeft: 25
                  }}
                >Urban</Text>
              </Left>

              <Right>
                <CheckBox
                  checked={this.state.checkboxTwo}
                  color='#22cade'
                  onPress={() => {
                    this.setState(state => {
                      if (state.interest.find(el => el == "Urban")) {
                        const index = state.interest.indexOf("Urban")
                        if (index > -1) {
                          state.interest.splice(index, 1)
                        }
                        return {
                          interest: state.interest,
                          checkboxTwo: !this.state.checkboxTwo
                        }
                      } else {
                        const list = state.interest.concat("Urban")
                        return {
                          interest: list,
                          checkboxTwo: !this.state.checkboxTwo
                        }
                      }
                    })
                  }}
                />
              </Right>

            </List>

            <List
              style={{
                width: Dimensions.get('window').width * 4.25 / 5,
                justifyContent: 'center',
                alignItems: 'center',
                borderColor: '#5edfff',
                flexDirection: 'row',
                marginVertical: 5
              }}
            >
              <Left>
                <Text
                  style={{
                    color: '#ecfcff',
                    fontSize: 18,
                    marginLeft: 25,
                  }}
                >Religious</Text>
              </Left>

              <Right>
                <CheckBox
                  checked={this.state.checkboxThree}
                  color='#22cade'
                  onPress={() => {
                    this.setState(state => {
                      if (state.interest.find(el => el == "Religious")) {
                        const index = state.interest.indexOf("Religious")
                        if (index > -1) {
                          state.interest.splice(index, 1)
                        }
                        return {
                          interest: state.interest,
                          checkboxThree: !this.state.checkboxThree
                        }
                      } else {
                        const list = state.interest.concat("Religious")
                        return {
                          interest: list,
                          checkboxThree: !this.state.checkboxThree
                        }
                      }
                    })
                  }}
                />
              </Right>

            </List>

            <List
              style={{
                width: Dimensions.get('window').width * 4.25 / 5,
                justifyContent: 'center',
                alignItems: 'center',
                borderColor: '#5edfff',
                flexDirection: 'row',
                marginVertical: 5
              }}
            >
              <Left>
                <Text
                  style={{
                    color: '#ecfcff',
                    fontSize: 18,
                    marginLeft: 25,
                  }}
                >Historical</Text>
              </Left>

              <Right>
                <CheckBox
                  checked={this.state.checkboxFour}
                  color='#22cade'
                  onPress={() => {
                    this.setState(state => {
                      if (state.interest.find(el => el == "Historical")) {
                        const index = state.interest.indexOf("Historical")
                        if (index > -1) {
                          state.interest.splice(index, 1)
                        }
                        return {
                          interest: state.interest,
                          checkboxFour: !this.state.checkboxFour
                        }
                      } else {
                        const list = state.interest.concat("Historical")
                        return {
                          interest: list,
                          checkboxFour: !this.state.checkboxFour
                        }
                      }
                    })
                  }}
                />
              </Right>

            </List>

            <List
              style={{
                width: Dimensions.get('window').width * 4.25 / 5,
                justifyContent: 'center',
                alignItems: 'center',
                borderColor: '#5edfff',
                flexDirection: 'row',
                marginVertical: 5
              }}
            >
              <Left>
                <Text
                  style={{
                    color: '#ecfcff',
                    fontSize: 18,
                    marginLeft: 25,
                  }}
                >Cultural</Text>
              </Left>

              <Right>
                <CheckBox
                  checked={this.state.checkboxFive}
                  color='#22cade'
                  onPress={() => {
                    this.setState(state => {
                      if (state.interest.find(el => el == "Cultural")) {
                        const index = state.interest.indexOf("Culturala")
                        if (index > -1) {
                          state.interest.splice(index, 1)
                        }
                        return {
                          interest: state.interest,
                          checkboxFive: !this.state.checkboxFive
                        }
                      } else {
                        const list = state.interest.concat("Cultural")
                        return {
                          interest: list,
                          checkboxFive: !this.state.checkboxFive
                        }
                      }
                    })
                  }}
                />
              </Right>

            </List>
            <List
              style={{
                width: Dimensions.get('window').width * 4.25 / 5,
                justifyContent: 'center',
                alignItems: 'center',
                borderColor: '#5edfff',
                flexDirection: 'row',
                marginVertical: 5
              }}
            >
              <Left>
                <Text
                  style={{
                    color: '#ecfcff',
                    fontSize: 18,
                    marginLeft: 25,
                  }}
                >Culinary</Text>
              </Left>

              <Right>
                <CheckBox
                  checked={this.state.checkboxSix}
                  color='#22cade'
                  onPress={() => {
                    this.setState(state => {
                      if (state.interest.find(el => el == "Culinary")) {
                        const index = state.interest.indexOf("Culinary")
                        if (index > -1) {
                          state.interest.splice(index, 1)
                        }
                        return {
                          interest: state.interest,
                          checkboxSix: !this.state.checkboxSix
                        }
                      } else {
                        const list = state.interest.concat("Culinary")
                        return {
                          interest: list,
                          checkboxSix: !this.state.checkboxSix
                        }
                      }
                    })
                  }}
                />
              </Right>

            </List>
            <Item
              fixedLabel
              style={{
                width: Dimensions.get('window').width * 4.5 / 5,
                borderColor: '#5edfff'
              }}
            ></Item>
          </View>


          <Content>

            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                marginVertical: 20
              }}
            >

              <Button
                onPress={
                  () => {
                    switch (true) {
                      case this.state.email === '' && this.state.password === '' && this.state.name === '':
                        ToastAndroid.show('please fill the requeired field', ToastAndroid.SHORT);
                        break;
                      case this.state.name === '' && this.state.password === '':
                        ToastAndroid.show('name and password are requiered', ToastAndroid.SHORT);
                        break;
                      case this.state.email === '' && this.state.password === '':
                        ToastAndroid.show('email and password are requiered', ToastAndroid.SHORT);
                        break;
                      case this.state.email === '' && this.state.name === '':
                        ToastAndroid.show('email and name are requiered', ToastAndroid.SHORT);
                        break;
                      case this.state.email === '':
                        ToastAndroid.show('email is requiered', ToastAndroid.SHORT);
                        break;
                      case this.state.password === '' && this.state.password2==='':
                        ToastAndroid.show('password is requiered', ToastAndroid.SHORT);
                        break;
                      case this.state.name === '':
                        ToastAndroid.show('name is requiered', ToastAndroid.SHORT);
                        break;
                      case (!(this.state.password === this.state.password2)):
                        ToastAndroid.show('your password doesnt match', ToastAndroid.SHORT);
                        break;

                      default: this.signUp()
                    }
                  }}
                style={{
                  backgroundColor: '#22cade',
                  alignItems: 'center',
                  justifyContent: 'center',
                  width: Dimensions.get('window').width * 4.5 / 5,
                  borderRadius: 200 / 18
                }}
              >
                {
                  this.state.isLoading ?
                    (<View>
                      <ActivityIndicator animating={this.state.isLoading} size="large" color="#ffffff" />
                    </View>)
                    :
                    (<Text
                      style={{
                        color: '#ffffff',
                        fontWeight: 'bold',
                        fontSize: 17
                      }}
                    > SIGN UP </Text>)
                }

              </Button>

            </View>

          </Content>
        </ScrollView>
      </Container>
    )
  }
}

export default withNavigation(SignUp)
