import React, { Component } from 'react'
import { Container, Fab, Icon, } from 'native-base'
import { withNavigation } from 'react-navigation'
import AsyncStorage from '@react-native-community/async-storage';
import ListStory from '../component/ListStory';

class Story extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLogin: false,
            token: 'null'
        }
    }

    componentDidMount() {
        this.userIsSignIn()
        setInterval(this.userIsSignIn, 1000)
    }
    userIsSignIn = async () => {
        const data = await AsyncStorage.getItem('@token')
        this.setState({ isLogin: data })
    }
    render() {
        if (!(this.state.isLogin == 'null' || this.state.isLogin == null)) {
            return (
                <Container style={{ backgroundColor: '#faffff' }}>
                    <ListStory/>
                    <Fab
                        style={{
                            backgroundColor: '#22cade',
                        }}
                        onPress={() => this.props.navigation.navigate('AddStory')}>
                        <Icon name="add" />
                    </Fab>
                </Container>
            )
        } else
            return (
                <Container style={{ backgroundColor: '#faffff' }}>
                    <ListStory />
                </Container>)
    }
}
export default withNavigation(Story)