import React, { Component } from 'react'
import { Text, View, Image, ScrollView } from 'react-native'
import { Container, Spinner, Header, Left, Right, Body, Icon, Title, Button, Item } from 'native-base';
import { withNavigation } from 'react-navigation';
import Axios from 'axios'
import Header2 from '../component/Header2';
class StoryDetail extends Component {
    constructor(props) {
        super(props)
        this.state = {
            id: '',
            title: '',
            photo: '',
            date: '',
            category: '',
            body: '',
            isLoading: false,
            location: ''
        }
    }

    componentDidMount() {
        this.getStoryById()
    }

    getStoryById = async () => {
        this.setState({ isLoading: true })
        const id = await this.props.navigation.getParam('id', '');
        try {
            const getStory = async () => await Axios.get(`https://glints-redu.herokuapp.com/api/stories/detail/${id}`, {
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            getStory()
                .then(res => {
                    this.setState({
                        title: res.data.story.title,
                        photo: res.data.story.photo,
                        date: res.data.story.date,
                        category: res.data.story.category,
                        body: res.data.story.body,
                        location: res.data.story.location,
                        isLoading: false
                    })
                })
                .catch(err => {
                    this.setState({ isLoading: false })
                })
        } catch (e) {
        }
    }

    render() {

        const newDate = new Date(this.state.date);
        const dateDay = newDate.toDateString().substring(0, 3);
        const dateDayDate = newDate.toDateString().substring(8, 10);
        const dateMonth = newDate.toDateString().substring(4, 7);
        const dateYear = newDate.toDateString().substring(11, 15);
        const upWord = this.state.category.toUpperCase().substring(0, 1)
        const downWord = this.state.category.substring(1, 7)

        return (
            <Container>

                <Header2/>
                <ScrollView>
                    {
                        this.state.isLoading ?

                            (<Spinner />)


                            :


                            (<View
                                style={{
                                    marginHorizontal: 20
                                }}
                            >
                                <Item><Text
                                    style={{
                                        fontSize: 20,
                                        fontWeight: 'bold',
                                        marginVertical: 10,
                                        fontFamily: 'Roboto',
                                        color: '#015249'
                                    }}
                                >{this.state.title}</Text>
                                </Item>
                                <Item><Image
                                    style={{
                                        width: '100%',
                                        height: 300,
                                        marginVertical: 10,
                                        borderRadius: 200 / 15
                                    }}
                                    source={{ uri: this.state.photo ? this.state.photo.secure_url : null }}
                                />
                                </Item>
                                <Item style={{
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}>
                                    <Icon
                                        type='Entypo'
                                        name='location-pin'
                                        style={{
                                            color: '#015249'
                                        }}
                                    />
                                    <Text
                                        style={{
                                            fontSize: 20,
                                            fontWeight: 'bold',
                                            marginVertical: 10,
                                            fontFamily: 'Roboto',
                                            color: '#015249'
                                        }}
                                    >{this.state.location}</Text>
                                </Item>
                                <Item>
                                    <Text
                                        style={{
                                            marginVertical: 5
                                        }}
                                    >{this.state.body}</Text></Item>
                                <Item >
                                    <Left><Text>category: </Text></Left>
                                    <Right><Text>{upWord + downWord}</Text></Right>
                                </Item>
                                <Item>
                                    <Left><Text>Posted: </Text></Left>
                                    <Right><Text>{dateDay + ', ' + dateDayDate + ' ' + dateMonth + ' ' + dateYear}</Text></Right>
                                </Item>

                            </View>)
                    }
                </ScrollView>
            </Container>
        )
    }
}
export default withNavigation(StoryDetail)