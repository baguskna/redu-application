import React, { Component } from 'react'
import { Text, View, Image, FlatList, Dimensions, ActivityIndicator, StyleSheet } from 'react-native'
import { Container, CardItem, Card, Left, Fab, Button, Icon, Header, Right, Body, Title, Input, Item } from 'native-base'
import { withNavigation } from 'react-navigation'
import Axios from 'axios';
import { ScrollView } from 'react-native-gesture-handler';

const styles = StyleSheet.create({
    button1: {
        marginVertical: 10,
        marginHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'center',
        width: Dimensions.get('window').width * 1 / 5,
        backgroundColor: '#ddfffb',
    },
    text1: {
        color: '#ffffff',

    }

})

class StorySearch extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            isLogin: '',
            page: 1,
            refreshing: false,
            loading: false,
            category: '',
            isHidden: false,
            title: ''
        }
    }
    renderHeader = () => {
        return (
            <View>
                <Header style={{ backgroundColor: '#ffffff' }} searchBar rounded>
                    <Item>
                        <Input
                            value={this.state.title}
                            onChangeText={text => { this.setState({ title: text }) }}
                            placeholder="Search" />
                        <Button transparent
                            onPress={() => {
                                this.getDataStory(this.state.title)
                            }}>
                            <Icon name="ios-search" /></Button>
                    </Item>
                </Header>

                <ScrollView
                    horizontal={true}
                    style={{
                        marginVertical: 10,
                    }}
                >
                    <Button
                        small
                        bordered
                        rounded
                        style={styles.button1
                        }
                        onPress={() => this.props.navigation.navigate('StoryByCategory', { category: 'Nature' })}>
                        <Text>Nature</Text>
                    </Button>
                    <Button
                        small
                        bordered
                        rounded
                        style={styles.button1
                        }
                        onPress={() => this.props.navigation.navigate('StoryByCategory', { category: 'City' })}>
                        <Text>City</Text></Button>
                    <Button
                        small
                        bordered
                        rounded
                        style={styles.button1
                        }
                        onPress={() => this.props.navigation.navigate('StoryByCategory', { category: 'Culture' })}>
                        <Text>Culture</Text></Button>
                    <Button
                        small
                        bordered
                        rounded
                        style={styles.button1
                        }
                        onPress={() => this.props.navigation.navigate('StoryByCategory', { category: 'Historical' })}>
                        <Text>Historical</Text></Button>
                    <Button
                        small
                        bordered
                        rounded
                        style={styles.button1
                        }
                        onPress={() => this.props.navigation.navigate('StoryByCategory', { category: 'Religious' })}>
                        <Text>Religious</Text></Button>
                    <Button
                        small
                        bordered
                        rounded
                        style={styles.button1
                        }
                        onPress={() => this.props.navigation.navigate('StoryByCategory', { category: 'Culinary' })}>
                        <Text>Culinary</Text></Button>
                    <Button
                        small
                        bordered
                        rounded
                        style={styles.button1
                        }
                        onPress={() => this.props.navigation.navigate('StoryByCategory', { category: 'Tourism' })}>
                        <Text>Tourism</Text></Button>
                </ScrollView>
            </View>
        );
    };
    getDataStory = (title) => {
        try {

            const getStory = async () => await Axios.get(`https://glints-redu.herokuapp.com/api/stories/search?title=${title}`, {
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            this.setState({ loading: true });

            getStory()
                .then(res => {
                    this.setState({
                        data: [...this.state.data, ...res.data.data.docs],
                        refreshing: false,
                        loading: false
                    })

                })
                .catch(err => {
                })
        } catch (e) { }
    }

    renderFooter = () => {
        if (!this.state.loading) return null;

        return (
            <View
                style={{
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    borderColor: "#CED0CE"
                }}
            >
                <ActivityIndicator animating size="large" />
            </View>
        );
    }

    handleEnd = () => {
        this.setState(state => ({ page: state.page + 1 }), () => this.getDataStory());
    };

    userIsSignIn = async () => {
        const data = await AsyncStorage.getItem('@token')
        this.setState({ isLogin: data })
    }

    handleRefresh = () => {
        this.setState(
            {
                page: 1,
                refreshing: true,
                data: []
            },
            () => {
                this.getDataStory();
            }
        );
    };

    render() {
        return (
            <Container style={{ backgroundColor: '#faffff' }}>
                <Header
                    transparent
                    style={{
                        marginTop: -20,
                        backgroundColor: '#015249',
                    }}
                >
                    <Left
                        style={{
                            flex: 1
                        }}
                    >
                    </Left>
                    <Body style={{ flex: 1 }}>
                        <Title style={{ fontFamily: 'serif', fontWeight: 'bold' }}>
                            <Text style={{ fontSize: 30, color: '#22cade' }}>S</Text>
                            <Text style={{ fontSize: 20, color: '#ffffff' }}>EARCH</Text>
                        </Title>
                    </Body>
                    <Right style={{ flex: 1 }}>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.navigate('KontenSearch')}
                        >
                            <Text style={{ color: '#ffffff', fontWeight: 'bold' }}>To Article</Text>
                        </Button>
                    </Right>
                </Header>
                <FlatList
                    data={this.state.data}
                    renderItem={({ item }) => (
                        <View
                            key={item._id}
                            style={{ alignItems: 'center', justifyContent: 'center' }}>
                            <Card
                                style={{
                                    marginVertical: 10,
                                    borderRadius: 200 / 12,
                                    width: Dimensions.get('window').width * 0.95,
                                }}>


                                <CardItem
                                    button
                                    onPress={() => this.props.navigation.navigate('StoryDetail', { id: item._id })}
                                    style={{ borderRadius: 200 / 12 }}>
                                    <Text
                                        style={{
                                            fontSize: 18,
                                            fontFamily: 'Roboto',
                                            fontWeight: 'bold',
                                            color: '#015249'
                                        }}
                                    > {item.title} </Text>
                                </CardItem>


                                <CardItem
                                    cardBody
                                    button
                                    onPress={() => this.props.navigation.navigate('StoryDetail', { id: item._id, photo: item._user.photo.secure_url })}
                                    style={{ marginHorizontal: 10 }}
                                >
                                    <Image
                                        style={{
                                            width: '100%',
                                            height: 300,
                                            borderRadius: 200 / 12,
                                        }}
                                        source={{ uri: item.photo ? item.photo.secure_url : null }}
                                    />
                                </CardItem>

                                <CardItem
                                    button
                                    onPress={() => { this.props.navigation.navigate('UserDataById', { id: item._user._id }) }}
                                    style={{ borderRadius: 200 / 12 }}
                                >
                                    <Left>
                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                            <Image
                                                style={{
                                                    width: 25,
                                                    height: 25,
                                                    borderRadius: 25 / 2
                                                }}
                                                source={{ uri: item._user.photo.secure_url }}
                                            />
                                            <Text
                                                style={{
                                                    fontSize: 15,
                                                    fontFamily: 'Roboto',
                                                    fontWeight: 'bold',
                                                }}
                                            > {item._user.name} </Text>
                                        </View>
                                    </Left>
                                </CardItem>


                            </Card>
                        </View>
                    )}
                    keyExtractor={item => item._id}
                    onEndReached={this.handleEnd}
                    onEndReachedThreshold={0.01}
                    onRefresh={this.handleRefresh}
                    refreshing={this.state.refreshing}
                    ListHeaderComponent={this.renderHeader}
                    ListFooterComponent={this.renderFooter}
                />
            </Container>

        )

    }
}
export default withNavigation(StorySearch)