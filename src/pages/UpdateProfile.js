import React, { Component } from 'react';
import { withNavigation, NavigationActions } from 'react-navigation';
import { Text, ToastAndroid, Image, View, Dimensions, ActivityIndicator, TouchableHighlight, } from 'react-native';
import { Container, Button, Item, Header, Right, Left, Icon, Body, Title, Content, List, CheckBox } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import Axios from 'axios';
import ImagePicker from 'react-native-image-picker';
import { Input } from 'react-native-elements';
import Header3 from '../component/Header3';

class UpadateProfile extends Component {
  constructor(props) {
    super(props)
    this.state = {
      picture: '',
      email: '',
      name: '',
      dataProfile: '',
      isLoading: false,
      interest: [],
      checkboxOne: false,
      checkboxTwo: false,
      checkboxThree: false,
      checkboxFour: false,
      checkboxFive: false,
      checkboxSix: false,
      isLoading: false,
    }
  }


  updateProfilePicture = async () => {
    this.setState({ isLoading: true })
    const token = await AsyncStorage.getItem('@token')
    let formdata = new FormData();
    formdata.append("file", { uri: this.state.photo.uri, name: this.state.photo.fileName, type: 'image/jpeg' })
    try {
      const updatePicture = async (bodyParameter) => await Axios.put('https://glints-redu.herokuapp.com/api/users/pict', bodyParameter, {
        headers: {
          'Content-Type': 'multipart/form-data',
          'Authorization': `bearer ${token}`
        }
      });
      updatePicture(formdata)
        .then(response => {
          this.setState({
            isLoading: false
          })
        })
        .catch(err => {
          ToastAndroid.show(`Picture failed`, ToastAndroid.SHORT)
          this.setState({ isLoading: false })
        })
    }
    catch (e) {
    }
  }

  updateDataUser = async () => {
    this.setState({ isLoading: true })
    const token = await AsyncStorage.getItem('@token')
    try {
      const editData = async (bodyParameter) => await Axios.put('https://glints-redu.herokuapp.com/api/users/update', bodyParameter, {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `bearer ${token}`
        }
      });
      editData({
        email: this.state.email,
        name: this.state.name,
        interest: this.state.interest
      })
        .then(res => {
          ToastAndroid.show('Update Success', ToastAndroid.SHORT)
          this.setState({ isLoading: false })
          this.props.navigation.reset([NavigationActions.navigate({ routeName: 'Profile' })], 0);
        })
        .catch(err => {
          ToastAndroid.show('Update Failed', ToastAndroid.SHORT)
          this.setState({ isLoading: false })
        })
    }
    catch (e) {
    }
  }

  handleChoosePhoto = () => {
    const option = {
      noData: true
    }
    ImagePicker.launchImageLibrary(option, response => {
      if (response.uri) {
        this.setState({ photo: response });
        this.setState({ picture: response.uri })
      }
    })
  };

  buttonPostData = () => {
    this.updateProfilePicture()
    this.updateDataUser()
  }

  fetchDataProfile = () => {
    AsyncStorage.getItem('profile')
      .then(res => {
        let profileData = JSON.parse(res)
        this.setState({
          dataProfile: profileData,
          name: profileData.name,
          email: profileData.email,
          picture: profileData.photo.secure_url
        })
      })
  }

  componentDidMount() {
    this.fetchDataProfile()
  }

  render() {
    return (
      <Container>
        <Header3/>

        <Content>
          <View
            style={{
              flexDirection: 'column',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              marginVertical: 35
            }}
          >
            <TouchableHighlight
              style={{
                width: 200,
                height: 200,
                borderRadius: 100
              }}
              onPress={this.handleChoosePhoto}
            >
              <Image
                style={{
                  width: 200,
                  height: 200,
                  borderRadius: 100,
                }}
                source={{ uri: this.state.picture ? this.state.picture : null }}
              />
            </TouchableHighlight>
          </View>
          <View style={{ alignItems: 'center', justifyContent: 'center' }}>
            <Item
              fixedLabel
              style={{
                width: Dimensions.get('window').width * 4.5 / 5,
                borderColor: '#5edfff'
              }}
            >
              <Input
                placeholder='Name'
                value={this.state.name}
                onChangeText={(text) => this.setState({ name: text })}
              />
            </Item>
            <Item
              fixedLabel
              style={{
                width: Dimensions.get('window').width * 4.5 / 5,
                borderColor: '#5edfff'
              }}
            >
              <Input
                placeholder='Email'
                value={this.state.email}
                onChangeText={(text) => this.setState({ email: text })}
                keyboardType={'email-address'} />
            </Item>
            <Item
              fixedLabel
              style={{
                width: Dimensions.get('window').width * 4.5 / 5,
                borderColor: '#5edfff'
              }}
            ><Text
              style={{
                marginVertical: 10,
                fontSize: 18,
                color: '#3d3d3d',
              }}
            >Interest :</Text>
            </Item>
            <List
              style={{
                width: Dimensions.get('window').width * 4.25 / 5,
                justifyContent: 'center',
                alignItems: 'center',
                borderColor: '#5edfff',
                flexDirection: 'row',
                marginVertical: 5
              }}
            >
              <Left>
                <Text
                  style={{
                    color: '#3d3d3d',
                    fontSize: 18,
                    marginLeft: 25
                  }}
                >Nature</Text>
              </Left>

              <Right>
                <CheckBox
                  checked={this.state.checkboxOne}
                  color='#22cade'
                  onPress={() => {
                    this.setState(state => {
                      if (state.interest.find(el => el == "Nature")) {
                        const index = state.interest.indexOf("Nature")
                        if (index > -1) {
                          state.interest.splice(index, 1)
                        }
                        return {
                          interest: state.interest,
                          checkboxOne: !this.state.checkboxOne
                        }
                      } else {
                        const list = state.interest.concat("Nature")
                        return {
                          interest: list,
                          checkboxOne: !this.state.checkboxOne
                        }
                      }
                    })
                  }}
                />
              </Right>

            </List>

            <List
              style={{
                width: Dimensions.get('window').width * 4.25 / 5,
                justifyContent: 'center',
                alignItems: 'center',
                borderColor: '#5edfff',
                flexDirection: 'row',
                marginVertical: 5
              }}
            >
              <Left>
                <Text
                  style={{
                    color: '#3d3d3d',
                    fontSize: 18,
                    marginLeft: 25
                  }}
                >Urban</Text>
              </Left>

              <Right>
                <CheckBox
                  checked={this.state.checkboxTwo}
                  color='#22cade'
                  onPress={() => {
                    this.setState(state => {
                      if (state.interest.find(el => el == "Urban")) {
                        const index = state.interest.indexOf("Urban")
                        if (index > -1) {
                          state.interest.splice(index, 1)
                        }
                        return {
                          interest: state.interest,
                          checkboxTwo: !this.state.checkboxTwo
                        }
                      } else {
                        const list = state.interest.concat("Urban")
                        return {
                          interest: list,
                          checkboxTwo: !this.state.checkboxTwo
                        }
                      }
                    })
                  }}
                />
              </Right>

            </List>

            <List
              style={{
                width: Dimensions.get('window').width * 4.25 / 5,
                justifyContent: 'center',
                alignItems: 'center',
                borderColor: '#5edfff',
                flexDirection: 'row',
                marginVertical: 5
              }}
            >
              <Left>
                <Text
                  style={{
                    color: '#3d3d3d',
                    fontSize: 18,
                    marginLeft: 25,
                  }}
                >Religious</Text>
              </Left>

              <Right>
                <CheckBox
                  checked={this.state.checkboxThree}
                  color='#22cade'
                  onPress={() => {
                    this.setState(state => {
                      if (state.interest.find(el => el == "Religious")) {
                        const index = state.interest.indexOf("Religious")
                        if (index > -1) {
                          state.interest.splice(index, 1)
                        }
                        return {
                          interest: state.interest,
                          checkboxThree: !this.state.checkboxThree
                        }
                      } else {
                        const list = state.interest.concat("Religious")
                        return {
                          interest: list,
                          checkboxThree: !this.state.checkboxThree
                        }
                      }
                    })
                  }}
                />
              </Right>

            </List>

            <List
              style={{
                width: Dimensions.get('window').width * 4.25 / 5,
                justifyContent: 'center',
                alignItems: 'center',
                borderColor: '#5edfff',
                flexDirection: 'row',
                marginVertical: 5
              }}
            >
              <Left>
                <Text
                  style={{
                    color: '#3d3d3d',
                    fontSize: 18,
                    marginLeft: 25,
                  }}
                >Historical</Text>
              </Left>

              <Right>
                <CheckBox
                  checked={this.state.checkboxFour}
                  color='#22cade'
                  onPress={() => {
                    this.setState(state => {
                      if (state.interest.find(el => el == "Historical")) {
                        const index = state.interest.indexOf("Historical")
                        if (index > -1) {
                          state.interest.splice(index, 1)
                        }
                        return {
                          interest: state.interest,
                          checkboxFour: !this.state.checkboxFour
                        }
                      } else {
                        const list = state.interest.concat("Historical")
                        return {
                          interest: list,
                          checkboxFour: !this.state.checkboxFour
                        }
                      }
                    })
                  }}
                />
              </Right>

            </List>

            <List
              style={{
                width: Dimensions.get('window').width * 4.25 / 5,
                justifyContent: 'center',
                alignItems: 'center',
                borderColor: '#5edfff',
                flexDirection: 'row',
                marginVertical: 5
              }}
            >
              <Left>
                <Text
                  style={{
                    color: '#3d3d3d',
                    fontSize: 18,
                    marginLeft: 25,
                  }}
                >Cultural</Text>
              </Left>

              <Right>
                <CheckBox
                  checked={this.state.checkboxFive}
                  color='#22cade'
                  onPress={() => {
                    this.setState(state => {
                      if (state.interest.find(el => el == "Cultural")) {
                        const index = state.interest.indexOf("Culturala")
                        if (index > -1) {
                          state.interest.splice(index, 1)
                        }
                        return {
                          interest: state.interest,
                          checkboxFive: !this.state.checkboxFive
                        }
                      } else {
                        const list = state.interest.concat("Cultural")
                        return {
                          interest: list,
                          checkboxFive: !this.state.checkboxFive
                        }
                      }
                    })
                  }}
                />
              </Right>

            </List>
            <List
              style={{
                width: Dimensions.get('window').width * 4.25 / 5,
                justifyContent: 'center',
                alignItems: 'center',
                borderColor: '#5edfff',
                flexDirection: 'row',
                marginVertical: 5
              }}
            >
              <Left>
                <Text
                  style={{
                    color: '#3d3d3d',
                    fontSize: 18,
                    marginLeft: 25,
                  }}
                >Culinary</Text>
              </Left>

              <Right>
                <CheckBox
                  checked={this.state.checkboxSix}
                  color='#22cade'
                  onPress={() => {
                    this.setState(state => {
                      if (state.interest.find(el => el == "Culinary")) {
                        const index = state.interest.indexOf("Culinary")
                        if (index > -1) {
                          state.interest.splice(index, 1)
                        }
                        return {
                          interest: state.interest,
                          checkboxSix: !this.state.checkboxSix
                        }
                      } else {
                        const list = state.interest.concat("Culinary")
                        return {
                          interest: list,
                          checkboxSix: !this.state.checkboxSix
                        }
                      }
                    })
                  }}
                />
              </Right>

            </List>
            <Item
              fixedLabel
              style={{
                width: Dimensions.get('window').width * 4.5 / 5,
                borderColor: '#5edfff'
              }}
            ></Item>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                marginVertical: 20
              }}
            >
              <Button
                style={{
                  backgroundColor: '#22cade',
                  alignItems: 'center',
                  justifyContent: 'center',
                  width: Dimensions.get('window').width * 4.5 / 5,
                  borderRadius: 200 / 18
                }}
                onPress={() => {
                  this.buttonPostData()

                }}
              >
                {
                  this.state.isLoading ?
                    (<ActivityIndicator animating={this.state.isLoading} size="large" color="#ffffff" />)
                    :
                    (<Text
                      style={{
                        color: '#ffffff',
                        fontWeight: 'bold',
                        fontSize: 17
                      }}
                    > Post </Text>)
                }
              </Button>
            </View>
          </View>
        </Content>
      </Container>

    )
  }
}

export default withNavigation(UpadateProfile);