import React, { Component } from 'react'
import { Text, View, Image, ScrollView, Dimensions } from 'react-native'
import { withNavigation, FlatList } from 'react-navigation'
import AsyncStorage from '@react-native-community/async-storage'
import Axios from 'axios'
import { Container, Content, Header, Title, Spinner, Card, CardItem } from 'native-base';
import Header2 from '../component/Header2'

class UserDataById extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      name: '',
      photo: '',
      interest: [],
      isLoading: false,
      data: [],
      refreshing: false,
      page: 1
    }
  }

  getDataStory = async () => {
    try {

      const id = await this.props.navigation.getParam('id', '');
      const getStory = async () => await Axios.get(`https://glints-redu.herokuapp.com/api/stories/search?user=${id}`, {
        headers: {
          'Content-Type': 'application/json',
        }
      })
      this.setState({ loading: true });

      getStory()
        .then(res => {
          this.setState({
            data: res.data.data.docs,
            refreshing: false,
            loading: false
          })
        })
        .catch(err => {
        })
    } catch (e) { }
  }
  renderFooter = () => {
    if (!this.state.loading) return null;
    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE"
        }}
      >
        <ActivityIndicator animating size="large" />
      </View>
    );
  }
  componentDidMount() {
    this.getDataUser()
    this.getDataStory()
  }

  getDataUser = async () => {
    this.setState({ isLoading: true })
    try {
      const id = await this.props.navigation.getParam('id', '')
      const token = await AsyncStorage.getItem('@token')
      const getUser = async () => await Axios.get(`https://glints-redu.herokuapp.com/api/users/detail/${id}`, {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `bearer ${token}`
        }
      })
      getUser()
        .then(res => {
          this.setState({
            email: res.data.data.email,
            interest: res.data.data.interest,
            name: res.data.data.name,
            photo: res.data.data.photo,
            isLoading: false
          })
        })
        .catch(err => {
        })
    } catch (e) { }
  }


  renderHeader = () => {
    return (
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          display: 'flex',
          marginVertical: 40
        }}
      >
        <Image
          style={{
            width: 100,
            height: 100,
            borderRadius: 100 / 2
          }}
          source={{ uri: this.state.photo.secure_url }}
        />

        <Text
          style={{
            fontWeight: 'bold',
            fontSize: 20,
            marginVertical: 10
          }}
        > {this.state.name} </Text>

        <Text>{this.state.email}</Text>

        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            marginVertical: 10
          }}
        >
          <Text>Interest : {this.state.interest}</Text>
        </View>
      </View>
    );
  };
  handleRefresh = () => {
    this.setState(
      {
        page: 1,
        refreshing: true,
        data: []
      },
      () => {
        this.getDataStory();
      }
    );
  };

  render() {
    return (
      <View>
        <Header2 />
        {
          this.state.isLoading ?

            (<Spinner />)

            :

            (
              <FlatList
                data={this.state.data}
                renderItem={({ item }) => (
                  <View
                    key={item._id}
                    style={{ alignItems: 'center', justifyContent: 'center' }}>
                    <Card
                      style={{
                        marginVertical: 10,
                        borderRadius: 200 / 12,
                        width: Dimensions.get('window').width * 0.95,
                      }}>


                      <CardItem
                        button
                        onPress={() => this.props.navigation.navigate('StoryDetail', { id: item._id })}
                        style={{ borderRadius: 200 / 12 }}>
                        <Text
                          style={{
                            fontSize: 18,
                            fontFamily: 'Roboto',
                            fontWeight: 'bold',
                            color: '#015249'
                          }}
                        > {item.title} </Text>
                      </CardItem>


                      <CardItem
                        cardBody
                        button
                        onPress={() => this.props.navigation.navigate('StoryDetail', { id: item._id, photo: item._user.photo.secure_url })}
                        style={{ marginHorizontal: 10 }}
                      >
                        <Image
                          style={{
                            width: '100%',
                            height: 300,
                            borderRadius: 200 / 12,
                          }}
                          source={{ uri: item.photo ? item.photo.secure_url : null }}
                        />
                      </CardItem>
                    </Card>
                  </View>
                )}
                keyExtractor={item => item._id}
                onRefresh={this.handleRefresh}
                refreshing={this.state.refreshing}
                ListHeaderComponent={this.renderHeader}
              />
            )
        }
      </View>
    )
  }
}

export default withNavigation(UserDataById);