import {
    REQUEST_USER_DATA,
    SHOW_USER_DATA,
    FAILED_GET_USER_DATA,
    SUCCESS_USER_DATA
} from '../type/UserType'
const initialState = {
    loading: true,
    interest: [],
    name: '',
    email: '',
    message: null,
    photo: ''
}
export default (state = initialState, action) => {
    switch (action.type) {
        case REQUEST_USER_DATA:
            return {
                ...state,
                loading: true
            }
        case SHOW_USER_DATA:
            return {
                ...state,
                loading: false,
                interest: action.payload.interest,
                name: action.payload.name,
                email: action.payload.email,
                photo: action.payload.photo
            }
        case FAILED_GET_USER_DATA:
            return {
                ...state,
                loading: false,
                message: action.payload
            }
        case SUCCESS_USER_DATA:
            return {
                ...state,
                loading: false,
                message: action.payload
            }
        default:
            return state
    }
} 