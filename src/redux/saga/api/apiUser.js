import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

export const apiGetUser = token => {
  let headers = {
    Authorization: `bearer ${token}`
  };
  return axios
    .get(
      'https://glints-redu.herokuapp.com/api/users/me',
      { headers: headers }
    )
    .then(res => {
      AsyncStorage.setItem('profile', JSON.stringify(res.data.data));
      return res.data.data;
    });
};
